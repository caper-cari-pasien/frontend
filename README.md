# This repository is a copy of our original team repository, which is private as it has been configured accordingly by our university.

# How to run
1. Install node and yarn
2. Clone repo
3. Run `yarn`
4. Run `yarn dev` to run in local
