import React from 'react'
import { DokterListModule } from '@modules'
import type { NextPage } from 'next'

const DokterList: NextPage = () => <DokterListModule />

export default DokterList
