import '../styles/globals.css'
import type { AppProps } from 'next/app'
import { LayoutModule } from '@modules'
import { AuthProvider } from '@contexts'
import { Toaster } from 'react-hot-toast'

export default function App({ Component, pageProps, router }: AppProps) {
  const excludeLayout = ['/login', '/register']

  if (excludeLayout.includes(router.pathname)) {
    return (
      <AuthProvider>
        <Toaster />
        <Component {...pageProps} />
      </AuthProvider>
    )
  }

  return (
    <AuthProvider>
      <LayoutModule>
        <Toaster />
        <Component {...pageProps} />
      </LayoutModule>
    </AuthProvider>
  )
}
