import React from 'react'
import { TreatmentDetailModule } from '@modules'
import type { NextPage } from 'next'

const DetailTreatment: NextPage = () => <TreatmentDetailModule />

export default DetailTreatment
