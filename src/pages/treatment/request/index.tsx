import React from 'react'
import { RequestTreatmentModule } from '@modules'
import type { NextPage } from 'next'

const RequestTreatment: NextPage = () => <RequestTreatmentModule />

export default RequestTreatment
