import React from 'react'
import { OpenTreatmentModule } from '@modules'
import type { NextPage } from 'next'

const OpenTreatment: NextPage = () => <OpenTreatmentModule />

export default OpenTreatment
