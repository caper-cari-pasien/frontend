import React from 'react'
import { ListOngoingModule } from '@modules'
import type { NextPage } from 'next'

const ListOngoing: NextPage = () => <ListOngoingModule />

export default ListOngoing
