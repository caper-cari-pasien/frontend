import React from 'react';
import { ChatModule } from '@modules';
import type { NextPage } from 'next';

const Chat: NextPage = () => <ChatModule />;

export default Chat;
