import React from 'react'
import { OngoingDetailModule } from '@modules'
import type { NextPage } from 'next'

const OngoingTreatment: NextPage = () => <OngoingDetailModule />

export default OngoingTreatment
