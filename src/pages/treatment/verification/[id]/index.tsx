import React from 'react'
import { VerificationDetailModule } from '@modules'
import type { NextPage } from 'next'

const VerificationDetail: NextPage = () => <VerificationDetailModule />

export default VerificationDetail
