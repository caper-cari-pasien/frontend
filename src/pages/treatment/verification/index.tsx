import React from 'react'
import { ListVerificationModule } from '@modules'
import type { NextPage } from 'next'

const ListVerification: NextPage = () => <ListVerificationModule />

export default ListVerification
