import React from 'react'
import { ListTreatmentModule } from '@modules'
import type { NextPage } from 'next'

const ListTreatment: NextPage = () => <ListTreatmentModule />

export default ListTreatment
