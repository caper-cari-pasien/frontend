import React from 'react'
import { RequestDetailModule } from '@modules'
import type { NextPage } from 'next'

const DetailRequest: NextPage = () => <RequestDetailModule />

export default DetailRequest
