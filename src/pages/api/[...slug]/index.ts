import axios, { HttpStatusCode } from 'axios'
import { NextApiRequest, NextApiResponse } from 'next'

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  try {
    console.log(req.method + ' ' + req.url)
    if (process.env.NEXT_PUBLIC_API_GATEWAY) {
      const result = await axios({
        method: req.method,
        url: process.env.NEXT_PUBLIC_API_GATEWAY + req.url,
        headers: req.headers,
        data: req.body,
      })

      res.status(result.status).send({
        ...result.data,
      })
    } else {
      res.status(HttpStatusCode.InternalServerError).send('API Gateway Not Set')
    }
  } catch (e: any) {
    console.log(e)
    res.status(e.response.status).send(e)
  }
}
