import { HttpStatusCode } from 'axios'
import { NextApiRequest, NextApiResponse } from 'next'
import httpProxyMiddleware from 'next-http-proxy-middleware'

// For preventing header corruption, specifically Content-Length header
export const config = {
  api: {
    bodyParser: false,
  },
}

export default async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    console.log(req.method + ' ' + req.url)
    if (process.env.NEXT_PUBLIC_API_GATEWAY) {
      const url = req.url
      console.log(url)

      const result = await httpProxyMiddleware(req, res, {
        target: process.env.NEXT_PUBLIC_API_GATEWAY,
        pathRewrite: [
          {
            patternStr: '^/api/upload',
            replaceStr: '/api',
          },
        ],
      })

      console.log(result)

      res.status(201).send(result)
    } else {
      res.status(HttpStatusCode.InternalServerError).send('API Gateway Not Set')
    }
  } catch (e: any) {
    console.log(e)
    res.status(e.response.status).send(e)
  }
}
