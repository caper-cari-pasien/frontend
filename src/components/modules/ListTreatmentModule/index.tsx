import Link from 'next/link'
import { Button } from '@elements'
import React, { useEffect, useState } from 'react'
import { toast } from 'react-hot-toast'
import { useAuthContext } from '@contexts'
import { User } from 'src/components/contexts/AuthContext/interface'

interface Treatment {
  id: string
  location: string
  treatmentTime: Date
  description: string
  price: number
  title: string
  category: Category
  doctor: Doctor
}
interface Category {
  id: string
  name: string
}
interface Doctor {
  id: string
  user: User
  sertifikatUrl: string
}

export const ListTreatmentModule: React.FC = () => {
  const { httpReq, user } = useAuthContext()
  const [listTreatment, setTreatment] = useState<Treatment[]>()

  const getListTreatment = async () => {
    try {
      const res = await httpReq({
        method: 'get',
        url: `/api/treatment`,
      })
      const count = Object.keys(res).length
      let fetchedList: Treatment[] = []

      console.log(res)
      for (let i = 0; i < count; i++) {
        fetchedList = [...fetchedList, res[i]]
      }

      setTreatment([...fetchedList])
    } catch (e: any) {
      console.log(e)
      toast.error('Maaf, telah terjadi kesalahan')
    }
  }
  useEffect(() => {
    getListTreatment()
  }, [user])

  return (
    <>
      <div className="min-h-screen flex flex-col justify-center items-center bg-carribean px-32 pt-32 py-12">
        <div className="bg-white rounded-lg w-full grow flex flex-col items-center py-12 px-12 gap-8">
          <div className="text-4xl mt-10">List Treatment</div>

          <div
            className="bg-black w-full grow flex flex-col mb-4"
            style={{ borderTop: '0.5px solid black' }}
          ></div>

          {listTreatment?.map((treatment) => (
            <div
              key={treatment.id}
              className="items-center bg-white rounded-2xl border border-black p-4 w-full max-w-[950px]"
              style={{ gridTemplateColumns: '1fr 2fr 1fr 1fr' }}
            >
              <div className="grid grid-cols-4 gap-3">
                <div className="col-span-1 flex items-center flex-shrink-0">
                  <img
                    className="w-full max-w-[120px] md:max-w-[200px] mb-4 md:mb-0 mr-0 md:mr-4 rounded-lg"
                    src="/images/usericon.png"
                    alt={`Picture of ${treatment.doctor.user.nama}`}
                  />
                </div>
                <div className="col-span-2 flex-grow w-full mt-8">
                  <div className="font-bold text-xl mb-2">
                    {treatment.title}
                  </div>
                  <div className="w-full flex gap-15">
                    <span className="whitespace">
                      Dokter&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                    <span className="whitespace-nowrap overflow-ellipsis overflow-hidden break-words truncate">
                      : {treatment.doctor.user.nama}
                    </span>
                  </div>
                  <div className="w-full flex gap-15">
                    <span className="whitespace">
                      Treatment&nbsp;&nbsp;&nbsp;
                    </span>
                    <span className="whitespace-nowrap overflow-ellipsis overflow-hidden break-words truncate">
                      : {treatment.category.name}
                    </span>
                  </div>
                  <div className="w-full flex gap-15">
                    <span className="whitespace">
                      Location&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                    <span
                      className={`
              ${
                treatment.location.length > 100
                  ? 'whitespace-nowrap overflow-ellipsis overflow-hidden break-words truncate'
                  : 'flex-wrap'
              }`}
                    >
                      {' '}
                      : {treatment.location}
                    </span>
                  </div>
                  <div className="w-full flex gap-15">
                    <span className="whitespace">Jam Praktik</span>
                    <span className="whitespace-nowrap overflow-ellipsis overflow-hidden break-words truncate">
                      {' '}
                      :{' '}
                      {`${new Date(
                        treatment.treatmentTime
                      ).getHours()}:${new Date(
                        treatment.treatmentTime
                      ).getMinutes()}`}
                    </span>
                  </div>

                  <div className="w-full flex gap-15">
                    <span className="whitespace">
                      Biaya&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                    <span className="whitespace-nowrap overflow-ellipsis overflow-hidden break-words truncate">
                      : {treatment.price}
                    </span>
                  </div>
                </div>
              </div>

              <div className="col-span-5 flex justify-end gap-3">
                {/* Detail button */}
                <div>
                  <Link
                    href={{
                      pathname: '/treatment/[id]',
                      query: { id: `${treatment.id}` },
                    }}
                  >
                    <div>
                      <Button className="bg-white shadow-lg mt-6 mr-4 w-40 align-self-end">
                        Detail
                      </Button>
                    </div>
                  </Link>
                </div>

                {/*                 
                Sebagai Pasien baru bisa daftar
                Daftar button -- POST */}

                {user?.role === 'PATIENT' && (
                  <Button
                    className=" shadow-lg mt-6 mr-4 w-40 align-self-end bg-sea-green text-white"
                    onClick={async () => {
                      try {
                        await httpReq({
                          method: 'post',
                          url: `/api/treatment/berjalan/register`,
                          body: {
                            username: `${user?.username}`,
                            openTreatmentId: `${treatment.id}`,
                            startTime: '2023-04-29T10:00:00.000+00:00',
                          },
                        })
                        // Handle the response as needed
                        toast.success('Berhasil pesan')
                      } catch (error) {
                        // Handle the error
                        console.error(error)
                      }
                    }}
                  >
                    Pesan
                  </Button>
                )}
              </div>
            </div>
          ))}
        </div>
      </div>
    </>
  )
}
