import { Footer, Navbar } from '@elements'
import React from 'react'
import { LayoutProps } from './interface'

export const LayoutModule: React.FC<LayoutProps> = ({ children }) => {
  return (
    <>
      <Navbar />
      <main className="min-h-screen font-roboto">{children}</main>
      <Footer />
    </>
  )
}
