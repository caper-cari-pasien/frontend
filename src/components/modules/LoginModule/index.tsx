import { useAuthContext } from '@contexts'
import { Button, InputText } from '@elements'
import Link from 'next/link'
import { SubmitHandler, useForm } from 'react-hook-form'
import { LoginFormData } from './interface'
import { useState } from 'react'
import toast from 'react-hot-toast'
import { useRouter } from 'next/router'

export const LoginModule = () => {
  const { httpReq, setUser } = useAuthContext()
  const { register, handleSubmit } = useForm<LoginFormData>()
  const [isLoggingIn, setIsLoggingIn] = useState(false)
  const router = useRouter()

  const login: SubmitHandler<LoginFormData> = async (data) => {
    try {
      setIsLoggingIn(true)

      const res = await httpReq({
        method: 'post',
        url: '/api/auth/login',
        body: data,
      })

      console.log(res.user)
      setUser({ ...res.user })
      localStorage.setItem('token', res.token)

      toast.success('Berhasil login')

      router.push('/')
    } catch (e: any) {
      console.log(e)
      if (e.response.status === 400) {
        toast.error('Username atau password salah')
      } else {
        toast.error('Maaf, telah terjadi kesalahan')
      }
    }
    setIsLoggingIn(false)
  }

  return (
    <div className="h-screen bg-carribean py-20 px-40 flex place-content-center">
      <div className="flex flex-col justify-center">
        <form
          className="w-full bg-white rounded-3xl items-center py-16 px-36"
          onSubmit={handleSubmit(login)}
        >
          <p className="text-3xl font-semibold text-raspberry flex place-content-center pb-6">
            Login
          </p>
          <div className="flex flex-col gap-2 md:gap-20 pt-2 place-content-center">
            <InputText
              label="Username"
              {...register('username', { required: true })}
            />
          </div>
          <div className="flex flex-col gap-2 md:gap-20 pt-2 place-content-center">
            <InputText
              label="Password"
              type="password"
              {...register('password', { required: true })}
            />
          </div>
          <div className="flex place-content-center pt-10">
            <Button
              className="bg-raspberry text-white h-10 w-80"
              disabled={isLoggingIn}
            >
              Login
            </Button>
          </div>
          <div className="flex place-content-center pt-2">
            <Link href="/register">
              <Button className="bg-sea-green text-white h-10 w-80">
                Don&apos;t have an account? Register
              </Button>
            </Link>
          </div>
        </form>
      </div>
    </div>
  )
}
