import React, { useState } from 'react'
import { useRouter } from 'next/router'
import Image from 'next/image'
import { Button } from '@elements'
import Link from 'next/link'

interface VerificationDetail {
  id: string
  category: string
  location: string
  treatmentTime: Date
  description: string
  name: string
  imageSrc: string
  role: 'Doctor' | 'Patient'
  price: number
  note?: string // Doctor-specific field
}

const verifications: VerificationDetail[] = [
  {
    id: '1',
    category: 'Tooth Extraction',
    location: 'Bandung',
    treatmentTime: new Date('2022-05-12T09:00:00'),
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    name: 'Dr. John Smith',
    imageSrc: '/images/usericon.png',
    note: 'Body Large. Most fonts have a particular weight which corresponds to one of the numbers in Common weight name mapping. However some fonts, called variable fonts, can support a range of weights with a more or less fine granularity, and this can give the designer a much closer degree of control over the chosen weight.',
    role: 'Doctor',
    price: 150000,
  },
  {
    id: '2',
    category: 'Scalling',
    location: 'Bandung',
    treatmentTime: new Date('2023-05-13T15:30:00'),
    description: 'Pediatrics consultation',
    name: 'Dr. Jane Doe',
    imageSrc: '/images/usericon.png',
    role: 'Doctor',
    price: 750000,
  },
  {
    id: '3',
    category: 'Scalling',
    location: '456 Elm St.',
    treatmentTime: new Date('2023-05-18T10:00:00'),
    description: 'Saya pengen cuci karang gigi dok',
    name: 'Brown Smith',
    imageSrc: '/images/usericon.png',
    role: 'Patient',
    price: 750000,
  },
  {
    id: '4',
    category: 'Dental checkup',
    location: '123 Main St.',
    treatmentTime: new Date('2023-05-15T14:30:00'),
    description:
      'Gigi saya udah sakit dari 2 minggu lalu dok. Tapi setelah bercermin saya tidak lihat ada masalah apa-apa',
    name: 'Lily',
    imageSrc: '/images/usericon.png',
    role: 'Patient',
    price: 750000,
  },
]
export const VerificationDetailModule = () => {
  // specify role
  const userRole = verifications.filter((user) => user.role === 'Doctor')
  const router = useRouter()
  const { id } = router.query

  const ongoing = userRole.find((ongoing) => ongoing.id === id)

  if (!ongoing) {
    // Handle case when treatment is not found
    return (
      <div className="flex justify-center py-20 mt-20 text-3xl">
        Ongoing Detail not found
      </div>
    )
  }
  const [showPopup, setShowPopup] = useState(false)

  const togglePopup = () => {
    setShowPopup(!showPopup)
  }

  return (
    <>
      <div className="min-h-screen flex flex-col justify-center items-center bg-carribean px-32 pt-32 py-12">
        <div className="bg-white rounded-lg w-full grow flex flex-col items-center py-12 px-12 gap-8">
          <Image
            src="/images/usericon.png"
            alt="user"
            width={200}
            height={200}
          />

          <div className="w-full border-b-2 border-gray"></div>

          {/* Account detail */}
          <section className="flex flex-col items-center w-full gap-2 px-32">
            <h1 className="font-bold text-2xl">{ongoing.name}</h1>
            {/* <div className="col-span flex-grow w-full mt-8 justify-center px-30"> */}
            <div className="flex flex-col gap-4 w-full">
              <div className="w-full flex gap-20 mt-4">
                <span className="">Treatment </span>
                <span className="">: {ongoing.category}</span>
              </div>
              <div className="w-full flex gap-20 mt-4">
                <span className="whitespace">Location&nbsp;&nbsp;&nbsp;</span>
                <span className="flex-wrap">: {ongoing.location}</span>
              </div>
              <div className="w-full flex gap-20 mt-4">
                <span className="whitespace">Schedule&nbsp;&nbsp;</span>
                <span className="">: {ongoing.treatmentTime.toString()}</span>
              </div>

              <div className="w-full flex gap-20 mt-4">
                <span className="whitespace">
                  Biaya&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </span>
                <span className="">: {ongoing.price}</span>
              </div>
              <div className="w-full flex gap-20 mt-4">
                <span className="whitespace">Deskripsi&nbsp;&nbsp;</span>
                <span className="">: {ongoing.description}</span>
              </div>
            </div>
          </section>

          {/* Render different content based on the role */}
          {ongoing.role === 'Doctor' ? (
            // Doctor-specific content
            <section className="flex flex-col items-center w-full gap-2 px-32">
              <Button onClick={togglePopup}>Foto Sertifikat</Button>
              {showPopup && (
                // foto gigi
                <section className="flex flex-col items-center w-full gap-2">
                  <h1 className="font-bold text-2xl">Foto Sertifikat</h1>
                  <div className="bg-[#EFEFEF] w-80 h-44 rounded-lg"></div>
                </section>
              )}
            </section>
          ) : (
            // Patient-specific content
            <section>
              <section className="flex flex-col items-center w-full gap-2 px-32">
                <Button onClick={togglePopup}>Foto Gigi</Button>
                {showPopup && (
                  // foto gigi
                  <section className="flex flex-col items-center w-full gap-2">
                    <h1 className="font-bold text-2xl">Foto Gigi</h1>
                    <div className="bg-[#EFEFEF] w-80 h-44 rounded-lg"></div>
                  </section>
                )}
              </section>
            </section>
          )}

          <div className="w-full border-b-2 border-gray"></div>

          <div className="flex justify-between mt-10">
            {/* Back button */}
            <Link href={{ pathname: '/treatment/verification' }}>
              <Button className="bg-white shadow-lg w-40 mt-6 mr-4">
                Back
              </Button>
            </Link>
          </div>
        </div>
      </div>
    </>
  )
}
