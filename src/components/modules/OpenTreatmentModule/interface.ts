export interface OpenTreatmentFormData {
  location: string
  treatmentTime: string
  description: string
  price: number
  categoryId: string
  doctorId: String
  title: string
  doctorUsername: string
}

export type Category = {
  id: string
  name: string
}
