import { useRouter } from 'next/router'
import { Button, InputText } from '@elements'
import { SubmitHandler, useForm } from 'react-hook-form'
import { useAuthContext } from '@contexts'
import React, { useEffect, useState } from 'react'
import Image from 'next/image'
import { DetailRow } from '../ProfileModule/elements'
import { Category, OpenTreatmentFormData } from './interface'
import { toast } from 'react-hot-toast'

export const OpenTreatmentModule: React.FC = () => {
  const { register, handleSubmit } = useForm<OpenTreatmentFormData>()
  const { httpReq } = useAuthContext()
  const router = useRouter()
  const { user } = useAuthContext()
  const [currentUser, setCurrentUser] = useState(user)
  const [categories, setCategories] = useState<Category[]>([])

  const handleOpenTreatment: SubmitHandler<OpenTreatmentFormData> = async (
    data
  ) => {
    if (!user) return

    const [hour, minute] = data.treatmentTime.split(':')
    const date = new Date()
    date.setHours(parseInt(hour))
    date.setMinutes(parseInt(minute))
    date.setSeconds(0)

    data.treatmentTime = date.toISOString()
    data.doctorUsername = user.username

    // console.log(data)

    try {
      const res = await httpReq({
        method: 'post',
        url: '/api/treatment',
        body: data,
      })
      console.log(res)
      toast.success('Berhasil menambahkan treatment baru')
      router.push('/treatment')
    } catch (e: any) {
      toast.error('Gagal menambahkan treatment baru, cek lagi isi datanya')
      console.log(e)
    }
  }

  const fetchProfile = async () => {
    try {
      if (user) {
        const res = await httpReq({
          method: 'get',
          url: `/api/profile/dokter/${user.username}`,
        })
        console.log(res)
        setCurrentUser({ ...res.user })
      }
    } catch (e: any) {
      console.log(e)
    }
  }

  const fetchCategory = async () => {
    try {
      const res = await httpReq({
        method: 'get',
        url: `/api/treatment/category`,
      })

      const count = Object.keys(res).length
      let categoriesFetched: Category[] = []
      for (let i = 0; i < count; i++) {
        categoriesFetched = [...categoriesFetched, res[i]]
      }

      setCategories([...categoriesFetched])
    } catch (e: any) {
      console.log(e)
    }
  }

  useEffect(() => {
    user && fetchProfile()
    fetchCategory()
  }, [user])

  return (
    <>
      <div className="bg-carribean pt-32 py-12">
        <div className="flex items-start">
          <div className="sm flex flex-col w-2/5 items-center px-16 text-white">
            <Image
              src="/images/usericon.png"
              alt="user"
              width={150}
              height={150}
            />

            {/* Account detail */}

            <section className="flex flex-col items-center w-full gap-2 px-6">
              <h1 className="font-bold text-2xl my-4">{user?.nama}</h1>
              <div className="flex flex-col gap-4 w-full">
                <DetailRow
                  title="Username"
                  content={currentUser?.nama}
                  canEdited={false}
                />
                <DetailRow title="Domisili" content={currentUser?.domisili} />
                <DetailRow title="Umur" content={currentUser?.umur} />
              </div>
            </section>

            <div className="w-full border-b-2 border-white my-6"></div>

            {/* Foto Sertifikat */}
            <section className="flex flex-col items-center w-full gap-2">
              <h1 className="font-bold text-2xl mb-2">Sertifikat</h1>
              <div className="bg-[#EFEFEF] w-80 h-44 rounded-lg">
                {currentUser?.certificateUrl && (
                  <img src={currentUser.certificateUrl} />
                )}
              </div>
            </section>
          </div>

          {/* Open Treatment */}
          <form
            className="flex flex-col w-3/5 bg-white rounded-lg grow py-12 px-12 mr-12 gap-3"
            onSubmit={handleSubmit(handleOpenTreatment)}
          >
            <div className="text-3xl flex justify-center mb-4">
              Buka Treatment
            </div>

            {/* <div className="text-lg">Treatment</div>
            <input
              type="text"
              className="w-full rounded-[7px] border border-black px-3 py-2.5"
              placeholder="Judul Treatment..."
            /> */}
            <InputText
              className="w-full rounded-[7px] px-3 py-2.5"
              label="Nama Treatment"
              type="text"
              {...register('title', { required: true })}
            />

            {/* <div className="text-lg">Deskripsi</div>
            <input
              type="text"
              className="w-full rounded-[7px] border border-black px-3 py-2.5"
              placeholder="Deskripsi Treatment..."
            /> */}
            <InputText
              className="w-full rounded-[7px] px-3 py-2.5"
              label="Deskripsi Treatment"
              type="text"
              {...register('description', { required: true })}
            />

            {/* <div className="text-lg mt-4">Lokasi Treatment</div>
            <input
              type="text"
              className="w-full rounded-[7px] border border-black px-3 py-2.5"
              placeholder="Provinsi"
            /> */}
            <InputText
              className="w-full rounded-[7px] px-3 py-2.5"
              label="Lokasi"
              type="text"
              {...register('location', { required: true })}
            />

            {/* <input
              type="text"
              className="w-full rounded-[7px] border border-black px-3 py-2.5"
              placeholder="Detail Lainnya"
            /> */}
            <InputText
              className="w-full rounded-[7px] px-3 py-2.5"
              label="Detail Lainnya"
              type="text"
              {...register('location', { required: false })}
            />

            {/* <div className="text-lg mt-4">Jam Treatment</div>
            <input
              type="text"
              className="w-full rounded-[7px] border border-black px-3 py-2.5"
              placeholder="Isi Jam Treatment..."
            /> */}
            {/* <InputText
              className="w-full rounded-[7px] px-3 py-2.5"
              label="Jam Treatment"
              type="text"
              {...register('treatmentTime', { required: true })}
            /> */}
            <label>
              <span className="text-raspberry">Waktu Treatment</span>
              <input
                type="time"
                className="w-full rounded-[7px] px-3 py-2.5 border"
                {...register('treatmentTime', { required: true })}
              />
            </label>

            {/* <div className="text-lg mt-4">Biaya</div>
            <input
              type="text"
              className="w-full rounded-[7px] border border-black px-3 py-2.5"
              placeholder="Isi Biaya..."
            /> */}
            <InputText
              className="w-full rounded-[7px] px-3 py-2.5"
              label="Biaya"
              type="number"
              {...register('price', { required: true })}
            />

            <label>
              <span className="text-raspberry">Kategori</span>
              <select
                className="w-full rounded-[7px] px-3 py-2.5 border"
                {...register('categoryId', { required: true })}
              >
                {categories.map((category, i) => (
                  <option value={category.id} key={i}>
                    {category.name}
                  </option>
                ))}
              </select>
            </label>

            <Button className="bg-sea-green text-white shadow-lg w-full h-[50px] mt-4">
              Buka Lowongan
            </Button>
          </form>
        </div>
      </div>
    </>
  )
}
