import { Button } from '@elements'
import React, { useEffect, useState } from 'react'
import { useAuthContext } from '@contexts'
import toast from 'react-hot-toast'
import { User } from 'src/components/contexts/AuthContext/interface'
import Link from 'next/link'

interface Ongoing {
  id: string
  note: string
  isVerified: boolean
  startTime: Date
  openTreatment: OpenTreatment
}

interface OpenTreatment {
  id: string
  location: string
  treatmentTime: Date
  description: string
  price: number
  category: Category
  doctor: Doctor
}
interface Category {
  id: string
  name: string
}
interface Doctor {
  id: string
  user: User
  sertifikatUrl: string
}

export const ListOngoingModule: React.FC = () => {
  const { httpReq, user } = useAuthContext()
  const [listOngoing, setOngoing] = useState<Ongoing[]>()
  const [showButtons, setShowButtons] = useState<boolean[]>([])

  const getListOngoing = async () => {
    try {
      const res = await httpReq({
        method: 'get',
        url: `/api/treatment/berjalan`,
      })
      const count = Object.keys(res).length
      let fetchedList: Ongoing[] = []

      console.log(res)
      for (let i = 0; i < count; i++) {
        fetchedList = [...fetchedList, res[i]]
      }

      setOngoing([...fetchedList])
      listOngoing && setShowButtons(listOngoing.map(() => true))
    } catch (e: any) {
      console.log(e)
      toast.error('Maaf, telah terjadi kesalahan')
    }
  }
  useEffect(() => {
    getListOngoing()
  }, [user])
  return (
    <>
      <div className="min-h-screen flex flex-col justify-center items-center bg-carribean px-32 pt-32 py-12">
        <div className="bg-white rounded-lg w-full grow flex flex-col items-center py-12 px-12 gap-8">
          <div className="text-4xl mt-10">List Treatment Anda</div>

          <div
            className="bg-black w-full grow flex flex-col mb-4"
            style={{ borderTop: '1px solid black' }}
          ></div>

          {listOngoing?.map((ongoing, i) => (
            <div
              key={ongoing.id}
              className="items-center bg-white rounded-2xl border border-black p-4 w-full max-w-[950px]"
              style={{ gridTemplateColumns: '1fr 2fr 1fr 1fr' }}
            >
              <div className="grid grid-cols-4 gap-3">
                <div className="col-span-1 flex items-center flex-shrink-0">
                  <img
                    className="w-full max-w-[120px] md:max-w-[200px] mb-4 md:mb-0 mr-0 md:mr-4 rounded-lg"
                    src="/images/usericon.png"
                    alt={`Picture of ${ongoing.openTreatment.doctor.user.nama}`}
                  />
                </div>
                <div className="col-span-2 flex-grow w-full mt-8">
                  <div className="font-bold text-xl mb-2">
                    {ongoing.openTreatment.doctor.user.nama}
                  </div>
                  <div className="w-full flex gap-15">
                    <span className="">Treatment </span>
                    <span className="whitespace-nowrap overflow-ellipsis overflow-hidden break-words truncate">
                      : {ongoing.openTreatment.category.name}
                    </span>
                  </div>
                  <div className="w-full flex gap-15">
                    <span className="">Note </span>
                    <span className="whitespace-nowrap overflow-ellipsis overflow-hidden break-words truncate">
                      : {ongoing.note}
                    </span>
                  </div>
                  <div className="w-full flex gap-15">
                    <span className="">Start Time </span>
                    <span className="whitespace-nowrap overflow-ellipsis overflow-hidden break-words truncate">
                      : {ongoing.startTime.toString()}
                    </span>
                  </div>
                  <div className="w-full flex gap-15">
                    <span className="">Status Verifikasi</span>
                    <span className="whitespace-nowrap overflow-ellipsis overflow-hidden break-words truncate">
                      :{' '}
                      {ongoing.isVerified
                        ? 'Sudah di verifikasi Dokter'
                        : 'Belum di verifikasi Dokter'}
                    </span>
                  </div>
                </div>
              </div>

              <div className="col-span-5 flex justify-end gap-3">
                <div>
                  <div>
                    {/* Detail button */}
                    <div>
                      <Link
                        href={{
                          pathname: '/treatment/ongoing/[id]',
                          query: { id: `${ongoing.id}` },
                        }}
                      >
                        <div>
                          <Button className="bg-white shadow-lg mt-6 mr-4 w-40 align-self-end">
                            Detail
                          </Button>
                        </div>
                      </Link>
                    </div>

                    {/* Verifikasi button */}
                    {!listOngoing[i].isVerified && user?.role === 'DOCTOR' && (
                      <Button
                        className="bg-sea-green text-white shadow-lg mt-6 mr-4 w-40 align-self-end"
                        onClick={async () => {
                          try {
                            const res = await httpReq({
                              method: 'patch',
                              url: `/api/treatment/berjalan/verify/${ongoing.id}`,
                            })

                            const newOngoing = [...listOngoing]
                            newOngoing[i] = res
                            setOngoing(newOngoing)
                            toast.success('Berhasil verifikasi')

                            const newShowButtons = [...showButtons]
                            newShowButtons[i] = false
                            setShowButtons(newShowButtons)
                          } catch (error) {
                            // Handle the error
                            console.error(error)
                          }
                        }}
                      >
                        Verifikasi
                      </Button>
                    )}
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </>
  )
}
