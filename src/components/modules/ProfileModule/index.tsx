import { Button } from '@elements'
import Image from 'next/image'
import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import { DetailRow } from './elements'
import { useAuthContext } from '@contexts'
import { toast } from 'react-hot-toast'
import { User } from 'src/components/contexts/AuthContext/interface'

export const ProfileModule = () => {
  const router = useRouter()
  const { username } = router.query
  const [isEditing, setIsEditing] = useState(false)
  const { httpReq, user } = useAuthContext()
  const [profile, setProfile] = useState<User>()
  const [nama, setNama] = useState('')
  const [domisili, setDomisili] = useState('')
  const [umur, setUmur] = useState('')
  const [isLoading, setIsLoading] = useState(false)
  const [uploadedFile, setUploadedFile] = useState<File>()

  const getUserData = async () => {
    try {
      if (user === undefined || username === undefined) return

      if (user.username !== username) {
        router.push('/')
      } else {
        setIsLoading(true)

        const role = user?.role === 'DOCTOR' ? 'dokter' : 'pasien'
        const res = await httpReq({
          method: 'get',
          url: `/api/profile/${role}/${username}`,
        })

        setProfile({ certificateUrl: res.certificateUrl, ...res.user })
        setNama(res.user.nama)
        setDomisili(res.user.domisili)
        setUmur(res.user.umur)
        setIsLoading(false)
      }
    } catch (e: any) {
      console.log(e)
      toast.error('Maaf, telah terjadi kesalahan')
      setIsLoading(false)
    }
  }

  useEffect(() => {
    getUserData()
  }, [user])

  const saveEdit = async () => {
    try {
      if (user === undefined) return
      if (user.username !== username) {
        router.push('/')
      }
      const role = user?.role === 'DOCTOR' ? 'dokter' : 'pasien'

      let data

      if (role === 'dokter') {
        data = {
          user: {
            nama: nama,
            username: user.username,
            role: user.role,
            umur: umur,
            domisili: domisili,
          },
          certificateUrl: '',
        }
      } else {
        data = {
          user: {
            nama: nama,
            username: user.username,
            role: user.role,
            umur: umur,
            domisili: domisili,
          },
          teethPictureUrl: '',
        }
      }

      await httpReq({
        method: 'put',
        url: `/api/profile/${role}/update/${username}`,
        body: data,
      })

      toast.success('Berhasil menyimpan profile')

      setIsEditing(false)
    } catch (e: any) {
      console.log(e)
      toast.error('Maaf, telah terjadi kesalahan')
    }
  }

  const onFileInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    e.currentTarget.files && setUploadedFile(e.currentTarget.files[0])
  }

  const uploadImage = async () => {
    if (!uploadedFile) {
      toast.error('Pilih file terlebih dahulu')
      return
    }

    try {
      const role = user?.role === 'DOCTOR' ? 'dokter' : 'pasien'
      const formData = new FormData()
      formData.append('file', uploadedFile)

      toast.loading('Sedang memroses...')
      setIsLoading(true)

      await httpReq({
        method: 'patch',
        url: `/api/upload/profile/${role}/${user?.username}/image`,
        headers: {
          'Content-Type': 'multipart/form-data',
        },
        body: formData,
      })

      toast.success('Berhasil upload gambar')
      setIsLoading(false)
      router.reload()
    } catch (e: any) {
      console.log(e)
      toast.error('Maaf, telah terjadi kesalahan')
      setIsLoading(false)
    }
  }

  return (
    <div className="min-h-screen flex flex-col justify-center items-center bg-carribean px-32 pt-32 py-12">
      <div className="bg-white rounded-lg w-full grow flex flex-col items-center py-12 px-12 gap-8">
        <Image src="/images/usericon.png" alt="user" width={200} height={200} />

        <div className="w-full border-b-2 border-gray"></div>

        {/* Account detail */}
        <section className="flex flex-col items-center w-full gap-2 px-32">
          <h1 className="font-bold text-2xl">Akun</h1>
          <div className="flex flex-col gap-4 w-full">
            <DetailRow
              title="Nama"
              content={nama}
              isEditing={isEditing}
              onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                setNama(e.target.value)
              }}
            />
            <DetailRow
              title="Username"
              content={username as string}
              isEditing={isEditing}
              canEdited={false}
            />
            <DetailRow
              title="Domisili"
              content={domisili}
              isEditing={isEditing}
              onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                setDomisili(e.target.value)
              }}
            />
            <DetailRow
              title="Umur"
              content={umur}
              isEditing={isEditing}
              onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                setUmur(e.target.value)
              }}
            />
            <DetailRow
              title="Tipe Akun"
              content={profile?.role === 'DOCTOR' ? 'Dokter' : 'Pasien'}
              isEditing={isEditing}
              canEdited={false}
            />
          </div>
        </section>

        <div className="w-full border-b-2 border-gray"></div>

        {/* Foto gigi / sertifikat */}
        {profile?.role && (
          <>
            <section className="flex flex-col items-center w-full gap-6">
              <h1 className="font-bold text-2xl">
                {profile.role === 'PATIENT' ? 'Foto Gigi' : 'Sertifikat'}
              </h1>
              <div className="bg-[#EFEFEF] w-80 h-44 rounded-lg relative flex flex-col items-center justify-center gap-4 overflow-clip">
                {profile.certificateUrl ? (
                  <img src={profile.certificateUrl}></img>
                ) : (
                  <>
                    <span className="text-lg font-normal">
                      {profile.role === 'PATIENT'
                        ? 'Upload foto gigi Anda!'
                        : 'Upload sertifikat Anda!'}
                    </span>
                    <input type="file" onChange={onFileInputChange} />
                    <Button
                      className="bg-sea-green text-white"
                      onClick={uploadImage}
                      disabled={isLoading}
                    >
                      Upload
                    </Button>
                  </>
                )}
              </div>
              {profile.certificateUrl && (
                <>
                  <input type="file" onChange={onFileInputChange} />
                  <Button
                    className="bg-sea-green text-white"
                    onClick={uploadImage}
                    disabled={isLoading}
                  >
                    Upload
                  </Button>
                </>
              )}
            </section>
          </>
        )}

        {/* Edit button */}
        <div className="flex flex-row-reverse w-full gap-4">
          {isEditing ? (
            <>
              <Button
                className="bg-sea-green text-white shadow-lg w-40"
                onClick={() => saveEdit()}
                disabled={isLoading}
              >
                Save Changes
              </Button>
              <Button
                className="bg-white shadow-lg w-40"
                onClick={() => setIsEditing(false)}
                disabled={isLoading}
              >
                Cancel
              </Button>
            </>
          ) : (
            <Button
              className="text-white shadow-lg"
              onClick={() => setIsEditing(true)}
            >
              Edit Profile
            </Button>
          )}
        </div>
      </div>
    </div>
  )
}
