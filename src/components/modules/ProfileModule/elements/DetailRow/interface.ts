export interface DetailRowProps {
  title?: string
  content?: string | number
  isEditing?: boolean
  canEdited?: boolean
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void
}
