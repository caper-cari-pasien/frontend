import { InputText } from '@elements'
import { DetailRowProps } from './interface'

export const DetailRow: React.FC<DetailRowProps> = ({
  title,
  content,
  isEditing,
  onChange,
  canEdited = true,
}) => {
  return (
    <div className="w-full flex gap-20">
      <span className="font-medium w-1/4">{title}</span>
      {isEditing ? (
        <InputText
          value={content}
          disabled={!canEdited}
          onChange={onChange}
        ></InputText>
      ) : (
        <span className="before:content-[':'] before:mr-2 grow">{content}</span>
      )}
    </div>
  )
}
