export interface ProfileFormData {
  nama: string
  username: string
  role: string
  umur: number
  domisili: string
}
