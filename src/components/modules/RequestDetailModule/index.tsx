import React, { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import Image from 'next/image'
import { Button } from '@elements'
import Link from 'next/link'
import { User } from 'src/components/contexts/AuthContext/interface'
import { useAuthContext } from '@contexts'
import toast from 'react-hot-toast'

interface Request {
  id: string
  title: string
  location: string
  treatmentTime: Date
  description: string
  patient: Patient
}
interface Patient {
  id: String
  user: User
  teethPictureUrl: String
}

export const RequestDetailModule = () => {
  const router = useRouter()
  const { id } = router.query

  const { httpReq, user } = useAuthContext()
  const [request, setRequest] = useState<Request>()

  const getRequest = async () => {
    try {
      const res = await httpReq({
        method: 'get',
        url: `/api/treatment/pesan/${id}`,
      })

      console.log(res)
      setRequest(res)
    } catch (e: any) {
      console.log(e)
      toast.error('Request not found')
    }
  }
  useEffect(() => {
    getRequest()
  }, [user])

  return (
    <>
      <div className="min-h-screen flex flex-col justify-center items-center bg-carribean px-32 pt-32 py-12">
        <div className="bg-white rounded-lg w-full grow flex flex-col items-center py-12 px-12 gap-8">
          <Image
            src="/images/usericon.png"
            alt="user"
            width={200}
            height={200}
          />

          <div className="w-full border-b-2 border-gray"></div>

          {/* Account detail */}
          <section className="flex flex-col items-center w-full gap-2 px-32">
            <h1 className="font-bold text-2xl">{request?.patient.user.nama}</h1>
            {/* <div className="col-span flex-grow w-full mt-8 justify-center px-30"> */}
            <div className="flex flex-col gap-4 w-full">
              <div className="w-full flex gap-20 mt-4">
                <span className="">Treatment </span>
                <span className="">: {request?.title}</span>
              </div>
              <div className="w-full flex gap-20 mt-4">
                <span className="whitespace">Location&nbsp;&nbsp;&nbsp;</span>
                <span className="flex-wrap">: {request?.location}</span>
              </div>
              <div className="w-full flex gap-20 mt-4">
                <span className="whitespace">Schedule&nbsp;&nbsp;</span>
                <span className="">: {request?.treatmentTime.toString()}</span>
              </div>

              <div className="w-full flex gap-20 mt-4">
                <span className="whitespace">Deskripsi&nbsp;&nbsp;</span>
                <span className="">: {request?.description}</span>
              </div>
            </div>
          </section>

          <div className="flex justify-between mt-10">
            {/* Back button */}
            <Link href={{ pathname: '/treatment/pesan' }}>
              <Button className="bg-white shadow-lg w-40 mr-4">Back</Button>
            </Link>
          </div>
        </div>
      </div>
    </>
  )
}
