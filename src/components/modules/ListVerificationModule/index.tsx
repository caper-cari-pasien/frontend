import Link from 'next/link'
import { Button } from '@elements'
import React from 'react'

interface Verification {
  id: string
  name: string
  category: string
  imageSrc: string
  role: 'Doctor' | 'Patient'
}

const listVerification: Verification[] = [
  {
    id: '1',
    name: 'Dr. John Smith',
    category: 'Tooth Extraction',
    imageSrc: '/images/usericon.png',
    role: 'Doctor',
  },
  {
    id: '2',
    name: 'Dr. Jane Doe',
    category: 'Scalling',
    imageSrc: '/images/usericon.png',
    role: 'Doctor',
  },
  {
    id: '3',
    name: 'Brown Smith',
    category: 'Scalling',
    imageSrc: '/images/usericon.png',
    role: 'Patient',
  },
  {
    id: '4',
    name: 'Lily',
    category: 'Dental checkup',
    imageSrc: '/images/usericon.png',
    role: 'Patient',
  },
]

export const ListVerificationModule: React.FC = () => {
  // specify the role
  const userRole = listVerification.filter((user) => user.role === 'Doctor')
  return (
    <>
      <div className="min-h-screen flex flex-col justify-center items-center bg-carribean px-32 pt-32 py-12">
        <div className="bg-white rounded-lg w-full grow flex flex-col items-center py-12 px-12 gap-8">
          <div className="text-4xl mt-10">List Verifikasi</div>

          {/* <div className="bg-black rounded-lg w-full grow flex flex-col items-center py-0.25 px-12 gap-4 mt-8"></div>
           */}
          <div
            className="bg-black w-full grow flex flex-col mb-4"
            style={{ borderTop: '1px solid black' }}
          ></div>

          {userRole.map((treatment) => (
            <div
              key={treatment.name}
              className="items-center bg-white rounded-2xl border border-black p-4 w-full max-w-[950px]"
              style={{ gridTemplateColumns: '1fr 2fr 1fr 1fr' }}
            >
              <div className="grid grid-cols-4 gap-3">
                <div className="col-span-1 flex items-center flex-shrink-0">
                  <img
                    className="w-full max-w-[120px] md:max-w-[200px] mb-4 md:mb-0 mr-0 md:mr-4 rounded-lg"
                    src={treatment.imageSrc}
                    alt={`Picture of ${treatment.name}`}
                  />
                </div>
                <div className="col-span-2 flex-grow w-full mt-8">
                  <div className="font-bold text-xl mb-2">{treatment.name}</div>
                  <div className="w-full flex gap-15">
                    <span className="">Treatment </span>
                    <span className="whitespace-nowrap overflow-ellipsis overflow-hidden break-words truncate">
                      : {treatment.category}
                    </span>
                  </div>
                </div>
              </div>

              <div className="col-span-5 flex justify-end gap-3">
                {/* Detail button */}
                <div>
                  <Link
                    href={{
                      pathname: '/treatment/verification/[id]',
                      query: { id: `${treatment.id}` },
                    }}
                  >
                    <div>
                      <Button className="bg-white shadow-lg mt-6 mr-4 w-40 align-self-end">
                        Detail
                      </Button>
                    </div>
                  </Link>
                </div>
                {/* Verifikasi button */}
                <div>
                  <Link
                    href={{
                      pathname: '/treatment/ongoing/',
                      query: { id: `${treatment.id}` },
                    }}
                  >
                    <div>
                      <Button className="bg-sea-green text-white shadow-lg mt-6 mr-4 w-40 align-self-end">
                        Verifikasi
                      </Button>
                    </div>
                  </Link>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </>
  )
}
