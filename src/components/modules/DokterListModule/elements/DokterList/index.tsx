import { useEffect, useState } from 'react'
import { DokterCard } from '../DokterCard'
import { useAuthContext, useDokterListContext } from '@contexts'
import { DokterInfo } from 'src/components/contexts/DokterListContext/interface'
import { toast } from 'react-hot-toast'

export const DokterList = () => {
  const { httpReq } = useAuthContext()
  const { dokterList, setDokterList, selectedDomisiliFilter } =
    useDokterListContext()
  const [isLoading, setIsLoading] = useState(false)
  const [message, setMessage] = useState('Sedang mengambil data...')

  const fetchDokter = async () => {
    try {
      const res = await httpReq({
        method: 'get',
        url: '/api/profile/dokter/all',
      })

      const count = Object.keys(res).length
      let fetchedList: DokterInfo[] = []

      for (let i = 0; i < count; i++) {
        fetchedList = [...fetchedList, res[i].user]
      }

      setDokterList([...fetchedList])
      setIsLoading(false)
    } catch (e: any) {
      console.log(e)
      if (e.response && e.response.status !== 401) {
        setMessage('Gagal mengambil data')
        toast.error('Maaf, telah terjadi kesalahan')
      }
    }
  }

  useEffect(() => {
    setIsLoading(true)
    fetchDokter()
  }, [])

  return (
    <section className="flex flex-col gap-6 w-full">
      {isLoading && <span className="self-center">{message}</span>}
      {!isLoading &&
        dokterList?.map((dokter, i) => {
          if (
            selectedDomisiliFilter !== undefined &&
            selectedDomisiliFilter !== ''
          ) {
            if (dokter.domisili === selectedDomisiliFilter)
              return <DokterCard dokter={dokter} key={i} />
            return <></>
          }
          return <DokterCard dokter={dokter} key={i} />
        })}
    </section>
  )
}
