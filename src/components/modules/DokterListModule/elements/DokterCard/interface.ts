import { DokterInfo } from 'src/components/contexts/DokterListContext/interface'

export interface DokterCardProps {
  dokter: DokterInfo
}
