import { FaUserCircle } from 'react-icons/fa'
import { DokterCardProps } from './interface'

export const DokterCard: React.FC<DokterCardProps> = ({ dokter }) => {
  return (
    <div className="flex gap-12 py-8 px-6">
      <FaUserCircle className="w-24 h-24" />
      <div className="flex flex-col gap-2">
        <span className="text-xl font-semibold">{dokter.nama}</span>
        <span>{dokter.umur} tahun</span>
        <span>{dokter.domisili}</span>
      </div>
    </div>
  )
}
