import { useDokterListContext } from '@contexts'
import { useRef } from 'react'

export const DomisiliFilter = () => {
  const filterSelectRef = useRef<HTMLSelectElement>(null)
  const { domisiliList, setSelectedDomisiliFilter } = useDokterListContext()

  const changeDomisiliFilter = () => {
    setSelectedDomisiliFilter(filterSelectRef.current?.value)
  }

  return (
    <select
      className="rounded-md border py-[0.1em] px-[0.5em]"
      ref={filterSelectRef}
      onChange={changeDomisiliFilter}
    >
      <option value={''}>Semua</option>
      {domisiliList.map((domisili, i) => (
        <option key={i}>{domisili}</option>
      ))}
    </select>
  )
}
