import React from 'react'
import { DokterList } from './elements/DokterList'
import { DokterListContextProvider } from '@contexts'
import { DomisiliFilter } from './elements/DomisiliFilter'

export const DokterListModule: React.FC = () => {
  return (
    <div className="flex flex-col items-center gap-6 py-40 px-20">
      <DokterListContextProvider>
        <h1 className="font-bold text-4xl">List Dokter</h1>
        <div className="w-full border-b border-black" />
        <div className="flex w-full gap-12">
          <div className="flex flex-col w-1/3">
            <span>Filter berdasarkan domisili</span>
            <DomisiliFilter />
          </div>
          <DokterList />
        </div>
      </DokterListContextProvider>
    </div>
  )
}
