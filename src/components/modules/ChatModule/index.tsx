import React, { useEffect, useState } from 'react'
import { Button, InputText } from '@elements'
import { useRouter } from 'next/router'
import Link from 'next/link'
import { useAuthContext } from '@contexts'
import toast from 'react-hot-toast'
import { SubmitHandler, useForm } from 'react-hook-form'


interface TreatmentChat {
  id: string
  content : string
  time: Date
  user : User
}

interface User {
  id: string
  username: string
  nama: string
  role: 'Doctor' | 'Patient'
  domisili: string
  umur: number
}

interface FormTreatmentChat {
  content: string
  time: Date
  treatmentId: string
  senderId: String
  username: string
}

export const ChatModule: React.FC = () => {
  const router = useRouter()
  let id: string | undefined = Array.isArray(router.query.id) ? router.query.id[0] : router.query.id;
  const { register, handleSubmit } = useForm<FormTreatmentChat>()
  const { httpReq, user } = useAuthContext()
  const [isRegistering, setIsRegistering] = useState(false)
  const [listTreatmentChat, setTreatmentChat] = useState<TreatmentChat[]>()
  const [isModalOpen, setIsModalOpen] = useState(false); 
  
  const handleSend: SubmitHandler<FormTreatmentChat> = async (data) => {
    setIsRegistering(true)

    if (!user || !id) return
  
    data.treatmentId = id
    data.time = new Date()
    data.username = user.username
    data.senderId = user.id

    try {
      await httpReq({
        method: 'post',
        url: '/api/chat',
        body: data,
      })
      setIsModalOpen(true); 
    } catch (e: any) {
      toast.error('Gagal mengirim pesan!')
      console.log(e)
    }
    setIsRegistering(false)
  }

  const getListTreatmentChat = async () => {
    try {
      const res = await httpReq({
        method: 'get',
        url: `/api/chat/${id}`,
      })
      const count = Object.keys(res).length
      let fetchedList: TreatmentChat[] = []

      console.log(res)
      for (let i = 0; i < count; i++) {
        fetchedList = [...fetchedList, res[i]]
      }

      setTreatmentChat([...fetchedList])
    } catch (e: any) {
      console.log(e)
      toast.error('Maaf, telah terjadi kesalahan')
    }
  }
  useEffect(() => {
    getListTreatmentChat()
  }, [user])
 
  const toggleModal = () => {
    setIsModalOpen(!isModalOpen);
  };

  return (
    <>
      <div className="min-h-screen flex flex-col justify-center items-center bg-carribean px-32 pt-32 py-12">
        <div className="bg-white rounded-lg w-full grow flex flex-col items-center py-12 px-12 gap-8">
        <div className="text-4xl mt-10">Chat</div>
          <div className="w-full"> {/* Add w-full class here */}
            {listTreatmentChat?.map((chat) => (
              <div key={chat.id} className="m-8 items-start">
                 <p className={`text-2xl ${chat?.user.role === 'Doctor' ? 'text-reaspberry' : 'text-sea-green'}`}>
                {chat?.user.username}
              </p>
                <p>{chat.content}</p>
                <p className="text-xs text-gray-400 text-right">{chat?.time.toLocaleString()}</p> 
                <hr className="my-5 w-full border-gray-200" />
              </div>
            ))}
          </div>
              
          {/* Form */}
          <form
            className="flex flex-col w-3/5 bg-white rounded-lg grow py-12 px-12 mr-12 gap-3"
            onSubmit={handleSubmit(handleSend)}
          >
            <InputText
              className="w-full h-20 rounded-[7px]"
              type="text"
              {...register('content', { required: true })}
            />
              <Button className="w-full text-white rounded-[7px] px-3 py-2.5">
                Send
              </Button>
          </form>
          <Link  
              href={{
              pathname: '/treatment/ongoing'
            }}>
              <Button className="w-full text-white bg-sea-green  rounded-[7px] px-3 py-2.5">
                Back
              </Button>
            </Link>
        </div>
      </div>

      {/* Modal */}
      {isModalOpen && (
        <div className="modal">
          <div className="modal-content">
            <span className="close" onClick={toggleModal}>
              &times;
            </span>
            <p>Chat berhasil dikirim!</p>
            <Link  
             href={{
              pathname: '/treatment/ongoing/[id]/chat',
              query: { id: `${id}` },
            }}>
              <Button className="w-full text-white bg-sea-green  rounded-[7px] px-3 py-2.5">
                Ok
              </Button>
            </Link>
          </div>
        </div>
      )}
    </>
  );
};


