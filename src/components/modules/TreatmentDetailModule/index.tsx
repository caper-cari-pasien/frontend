import React, { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import Image from 'next/image'
import { Button } from '@elements'
import { toast } from 'react-hot-toast'
import Link from 'next/link'
import { useAuthContext } from '@contexts'
import { User } from 'src/components/contexts/AuthContext/interface'

interface Treatment {
  id: string
  location: string
  treatmentTime: Date
  description: string
  price: number
  category: Category
  doctor: Doctor
}
interface Category {
  id: string
  name: string
}
interface Doctor {
  id: string
  user: User
  sertifikatUrl: string
}

export const TreatmentDetailModule = () => {
  const router = useRouter()
  const { id } = router.query

  const { httpReq, user } = useAuthContext()
  const [treatment, setTreatment] = useState<Treatment>()

  const getTreatment = async () => {
    try {
      const res = await httpReq({
        method: 'get',
        url: `${process.env.NEXT_PUBLIC_API_GATEWAY}/api/treatment/${id}`,
      })

      console.log(res)
      setTreatment(res)
    } catch (e: any) {
      console.log(e)
      toast.error('Treatment not found')
    }
  }
  useEffect(() => {
    getTreatment()
  }, [user])

  return (
    <>
      <div className="min-h-screen flex flex-col justify-center items-center bg-carribean px-32 pt-32 py-12">
        <div className="bg-white rounded-lg w-full grow flex flex-col items-center py-12 px-12 gap-8">
          <Image
            src="/images/usericon.png"
            alt="user"
            width={200}
            height={200}
          />

          <div className="w-full border-b-2 border-gray"></div>

          {/* Account detail */}
          <section className="flex flex-col items-center w-full gap-2 px-32">
            <h1 className="font-bold text-2xl">
              {treatment?.doctor.user.nama}
            </h1>
            {/* <div className="col-span flex-grow w-full mt-8 justify-center px-30"> */}
            <div className="flex flex-col gap-4 w-full">
              <div className="w-full flex gap-20 mt-4">
                <span className="">Treatment </span>
                <span className="">: {treatment?.category.name}</span>
              </div>
              <div className="w-full flex gap-20 mt-4">
                <span className="whitespace">Location&nbsp;&nbsp;&nbsp;</span>
                <span className="flex-wrap">: {treatment?.location}</span>
              </div>
              <div className="w-full flex gap-20 mt-4">
                <span className="whitespace">Schedule&nbsp;&nbsp;</span>
                <span className="">
                  : {treatment?.treatmentTime.toString()}
                </span>
              </div>

              <div className="w-full flex gap-20 mt-4">
                <span className="whitespace">
                  Biaya&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </span>
                <span className="">: {treatment?.price}</span>
              </div>
              <div className="w-full flex gap-20 mt-4">
                <span className="whitespace">Deskripsi&nbsp;&nbsp;</span>
                <span className="">: {treatment?.description}</span>
              </div>
            </div>
          </section>

          <div className="flex justify-between mt-10">
            {/* Back button */}
            <Link href={{ pathname: '/treatment/' }}>
              <Button className="bg-white shadow-lg w-40 mt-6 mr-4">
                Back
              </Button>
            </Link>

            {/* Daftar button -- POST*/}

            <Button
              className=" shadow-lg mt-6 mr-4 w-40 align-self-end bg-sea-green text-white"
              onClick={async () => {
                try {
                  await httpReq({
                    method: 'post',
                    url: `/api/treatment/berjalan/register`,
                    body: {
                      username: `${user?.username}`,
                      openTreatmentId: `${treatment?.id}`,
                      startTime: '2023-04-29T10:00:00.000+00:00',
                    },
                  })
                  // Handle the response as needed
                  toast.success('Berhasil pesan')
                } catch (error) {
                  // Handle the error
                  console.error(error)
                }
              }}
            >
              Pesan
            </Button>
          </div>
        </div>
      </div>
    </>
  )
}
