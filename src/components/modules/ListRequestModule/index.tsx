import Link from 'next/link'
import { Button } from '@elements'
import React, { useEffect, useState } from 'react'
import { User } from 'src/components/contexts/AuthContext/interface'
import { useAuthContext } from '@contexts'
import toast from 'react-hot-toast'

interface Request {
  id: string
  title: string
  location: string
  treatmentTime: Date
  description: string
  patient: Patient
}
interface Patient {
  id: String
  user: User
  teethPictureUrl: String
}

export const ListRequestModule: React.FC = () => {
  const { httpReq, user } = useAuthContext()
  const [listRequest, setRequest] = useState<Request[]>()

  const getListRequest = async () => {
    try {
      const res = await httpReq({
        method: 'get',
        url: `/api/treatment/pesan`,
      })
      const count = Object.keys(res).length
      let fetchedList: Request[] = []

      console.log(res)
      for (let i = 0; i < count; i++) {
        fetchedList = [...fetchedList, res[i]]
      }

      setRequest([...fetchedList])
    } catch (e: any) {
      console.log(e)
      toast.error('Maaf, telah terjadi kesalahan')
    }
  }
  useEffect(() => {
    getListRequest()
  }, [user])

  return (
    <>
      <div className="min-h-screen flex flex-col justify-center items-center bg-carribean px-32 pt-32 py-12">
        <div className="bg-white rounded-lg w-full grow flex flex-col items-center py-12 px-12 gap-8">
          <div className="text-4xl mt-10">List Pengajuan Treatment</div>

          <div
            className="bg-black w-full grow flex flex-col mb-4"
            style={{ borderTop: '1px solid black' }}
          ></div>

          {listRequest?.map((request) => (
            <div
              key={request.id}
              className="items-center bg-white rounded-2xl border border-black p-4 w-full max-w-[950px]"
              style={{ gridTemplateColumns: '1fr 2fr 1fr 1fr' }}
            >
              <div className="grid grid-cols-4 gap-3">
                <div className="col-span-1 flex items-center flex-shrink-0">
                  <img
                    className="w-full max-w-[120px] md:max-w-[200px] mb-4 md:mb-0 mr-0 md:mr-4 rounded-lg"
                    src="/images/usericon.png"
                    alt={`Picture of ${request?.patient.user.nama}`}
                  />
                </div>
                <div className="col-span-2 flex-grow w-full mt-8">
                  <div className="font-bold text-xl mb-2">
                    {request?.patient.user.nama}
                  </div>
                  <div className="w-full flex gap-18">
                    <span className="whitespace">
                      Treatment&nbsp;&nbsp;&nbsp;
                    </span>
                    <span className="whitespace-nowrap overflow-ellipsis overflow-hidden break-words truncate">
                      : {request?.title}
                    </span>
                  </div>
                  <div className="w-full flex gap-18">
                    <span className="">
                      Location&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                    <span
                      className={`
                      ${
                        request?.location.length > 100
                          ? 'whitespace-nowrap overflow-ellipsis overflow-hidden break-words truncate'
                          : 'flex-wrap'
                      }`}
                    >
                      {' '}
                      : {request?.location}
                    </span>
                  </div>
                  <div className="w-full flex gap-18">
                    <span className="whitespace">
                      Schedule&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </span>
                    <span className="whitespace-nowrap overflow-ellipsis overflow-hidden break-words truncate">
                      {' '}
                      : {request?.treatmentTime.toString()}
                    </span>
                  </div>

                  <div className="w-full flex gap-18">
                    <span className="whitespace">Keterangan&nbsp;</span>
                    <span className="whitespace-nowrap overflow-ellipsis overflow-hidden break-words truncate">
                      : {request?.description}
                    </span>
                  </div>
                </div>
              </div>

              <div className="col-span-5 flex justify-end gap-3">
                {/* Detail button */}
                <div>
                  <Link
                    href={{
                      pathname: './pesan/[id]',
                      query: { id: `${request.id}` },
                    }}
                  >
                    <div>
                      <Button className="bg-sea-green text-white shadow-lg mr-4 w-40 align-self-end">
                        Detail
                      </Button>
                    </div>
                  </Link>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </>
  )
}
