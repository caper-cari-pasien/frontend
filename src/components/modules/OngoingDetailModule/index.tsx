import React, { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import Image from 'next/image'
import { Button, InputText } from '@elements'
import Link from 'next/link'
import { User } from 'src/components/contexts/AuthContext/interface'
import { useAuthContext } from '@contexts'
import toast from 'react-hot-toast'
import { DetailRow } from '../ProfileModule/elements'

interface Ongoing {
  id: string
  note: string
  isVerified: boolean
  startTime: Date
  patient: Patient
  openTreatment: OpenTreatment
}

interface OpenTreatment {
  id: string
  location: string
  treatmentTime: Date
  description: string
  price: number
  category: Category
  doctor: Doctor
  title: string
}
interface Category {
  id: string
  name: string
}
interface Doctor {
  id: string
  user: User
  sertifikatUrl: string
}

interface Patient {
  id: string
  user: User
  teethPictureUrl: string
}

export const OngoingDetailModule = () => {
  const router = useRouter()
  const { id } = router.query

  const { httpReq, user } = useAuthContext()
  const [ongoing, setOngoing] = useState<Ongoing>()
  const [showSertifikat, setShowSertifikat] = useState(false)
  const [showTeethPicture, setShowTeethPicture] = useState(false)
  const [isEditingNote, setIsEditingNote] = useState(false)
  const [noteValue, setNoteValue] = useState('')

  const getOngoing = async () => {
    console.log('tesssss')
    try {
      const res = await httpReq({
        method: 'get',
        url: `/api/treatment/berjalan/${id}`,
      })

      console.log(res)
      setOngoing(res)
      setNoteValue(res.note)
    } catch (e: any) {
      console.log(e)
      toast.error('Ongoing not found')
    }
  }

  useEffect(() => {
    id && getOngoing()
  }, [router.asPath])

  const toggleSertifikat = () => {
    setShowSertifikat(!showSertifikat)
  }

  const toggleTeethPicture = () => {
    setShowTeethPicture(!showTeethPicture)
  }

  const updateNote = async () => {
    try {
      if (ongoing && noteValue && noteValue !== 'Belum ada note') {
        console.log(noteValue)

        const res = await httpReq({
          method: 'patch',
          url: `/api/treatment/berjalan/${ongoing.id}/note`,
          body: {
            note: noteValue,
          },
        })

        setOngoing(res)
        setNoteValue(noteValue)
        toast.success('Berhasil mengubah note')
      }
    } catch (e: any) {
      toast.error('Maaf, telah terjadi kesalahan')
    }
    setIsEditingNote(false)
  }

  return (
    <>
      <div className="min-h-screen flex flex-col justify-center items-center bg-carribean px-32 pt-32 py-12">
        <div className="bg-white rounded-lg w-full grow flex flex-col items-center py-12 px-12 gap-8">
          <Image
            src="/images/usericon.png"
            alt="user"
            width={200}
            height={200}
          />
          <div className="flex justify-between">
            {/* Chat button */}
            <div>
              <Link
                href={{
                  pathname: '/treatment/ongoing/[id]/chat',
                  query: { id: `${ongoing?.id}` },
                }}
              >
                <div>
                  <Button className="bg-sea-green text-white hadow-lg mt-6 mr-4 w-40 align-self-end">
                    Chat
                  </Button>
                </div>
              </Link>
            </div>
          </div>

          <div className="w-full border-b-2 border-gray"></div>
          {/* Account detail for doctor */}
          <section className="flex flex-col items-center w-full gap-2 px-32">
            <h1 className="font-bold text-2xl">
              {ongoing?.openTreatment.doctor.user.nama}
            </h1>
            {/* <div className="col-span flex-grow w-full mt-8 justify-center px-30"> */}
            <div className="flex flex-col gap-4 w-full">
              <div className="w-full flex gap-20 mt-4">
                <span className="">Treatment </span>
                <span className="">: {ongoing?.openTreatment.title}</span>
              </div>
              <div className="w-full flex gap-20 mt-4">
                <span className="whitespace">Location&nbsp;&nbsp;&nbsp;</span>
                <span className="flex-wrap">
                  : {ongoing?.openTreatment.location}
                </span>
              </div>
              <div className="w-full flex gap-20 mt-4">
                <span className="whitespace">Schedule&nbsp;&nbsp;</span>
                <span className="">
                  : {ongoing?.openTreatment.treatmentTime.toString()}
                </span>
              </div>

              <div className="w-full flex gap-20 mt-4">
                <span className="whitespace">
                  Biaya&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </span>
                <span className="">: {ongoing?.openTreatment.price}</span>
              </div>
              <div className="w-full flex gap-20 mt-4">
                <span className="whitespace">Deskripsi&nbsp;&nbsp;</span>
                <span className="">: {ongoing?.openTreatment.description}</span>
              </div>
            </div>
          </section>

          <section className="flex flex-col items-center w-full gap-2 px-32">
            <div className="flex flex-col gap-4 w-full">
              <div className="w-1/2 flex gap-20 mt-4">
                <DetailRow
                  title="Note"
                  isEditing={isEditingNote}
                  content={noteValue ? noteValue : 'Belum ada note'}
                  onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                    setNoteValue(e.target.value)
                  }}
                />
              </div>
              {user?.role === 'DOCTOR' && ongoing?.isVerified && (
                <div className="flex gap-4">
                  {!isEditingNote && (
                    <Button
                      className="text-white"
                      onClick={() => setIsEditingNote(!isEditingNote)}
                    >
                      Edit Note
                    </Button>
                  )}
                  {isEditingNote && (
                    <>
                      <Button
                        className="text-white"
                        onClick={() => setIsEditingNote(!isEditingNote)}
                      >
                        Cancel
                      </Button>
                      <Button
                        className="bg-sea-green text-white"
                        onClick={updateNote}
                      >
                        Save
                      </Button>
                    </>
                  )}
                </div>
              )}
            </div>
          </section>

          <section className="flex flex-col items-center w-full gap-2 px-32">
            <Button onClick={toggleSertifikat} className="text-white">
              Foto Sertifikat
            </Button>
            {showSertifikat && (
              <section className="flex flex-col items-center w-full gap-2">
                <h1 className="font-bold text-2xl">Foto Sertifikat</h1>
                <div className="bg-[#EFEFEF] w-80 h-44 rounded-lg"></div>
              </section>
            )}
          </section>

          <div className="w-full border-b-2 border-gray"></div>
          {/* Account detail for Patient */}
          <Image
            src="/images/usericon.png"
            alt="user"
            width={200}
            height={200}
          />
          <section className="flex flex-col items-center w-full gap-2 px-32">
            <h1 className="font-bold text-2xl">{ongoing?.patient.user.nama}</h1>
            {/* <div className="col-span flex-grow w-full mt-8 justify-center px-30"> */}
            <div className="flex flex-col gap-4 w-full">
              <div className="w-full flex gap-20 mt-4">
                <span className="whitespace">
                  Umur&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                </span>
                <span className="">: {ongoing?.patient.user.umur}</span>
              </div>
            </div>
            <div className="flex flex-col gap-4 w-full">
              <div className="w-full flex gap-20 mt-4">
                <span className="whitespace">Domisili&nbsp;&nbsp;</span>
                <span className="">: {ongoing?.patient.user.domisili}</span>
              </div>
            </div>
          </section>
          <section>
            <section className="flex flex-col items-center w-full gap-2 px-32">
              <Button onClick={toggleTeethPicture} className="text-white">
                Foto Gigi
              </Button>
              {showTeethPicture && (
                // foto gigi
                <section className="flex flex-col items-center w-full gap-2">
                  <h1 className="font-bold text-2xl">Foto Gigi</h1>
                  <div className="bg-[#EFEFEF] w-80 h-44 rounded-lg"></div>
                </section>
              )}
            </section>
          </section>
          <div className="w-full border-b-2 border-gray"></div>
          <div className="flex justify-between mt-10">
            {/* Back button */}
            <Link href={{ pathname: '/treatment/ongoing' }}>
              <Button className="bg-white shadow-lg w-40 mt-6 mr-4">
                Back
              </Button>
            </Link>
          </div>
        </div>
      </div>
    </>
  )
}
