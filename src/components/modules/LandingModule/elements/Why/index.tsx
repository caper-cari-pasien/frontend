import WhyCard from './WhyCard'
import {
  TbCertificate,
  TbMedal,
  TbShieldCheck,
  TbLicense,
} from 'react-icons/tb'
import { ComponentPropsWithoutRef } from 'react'

const Wave1: React.FC<ComponentPropsWithoutRef<'svg'>> = ({ className }) => {
  return (
    <svg
      className={className}
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 1440 320"
    >
      <path
        fill="#006D77"
        fillOpacity="1"
        d="M0,224L80,229.3C160,235,320,245,480,213.3C640,181,800,107,960,74.7C1120,43,1280,53,1360,58.7L1440,64L1440,0L1360,0C1280,0,1120,0,960,0C800,0,640,0,480,0C320,0,160,0,80,0L0,0Z"
      ></path>
    </svg>
  )
}

const Wave2: React.FC<ComponentPropsWithoutRef<'svg'>> = ({ className }) => {
  return (
    <svg
      className={className}
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 1440 320"
    >
      <path
        fill="#006D77"
        fillOpacity="1"
        d="M0,128L80,106.7C160,85,320,43,480,37.3C640,32,800,64,960,80C1120,96,1280,96,1360,96L1440,96L1440,320L1360,320C1280,320,1120,320,960,320C800,320,640,320,480,320C320,320,160,320,80,320L0,320Z"
      ></path>
    </svg>
  )
}

export const Why = () => {
  return (
    <section
      className="min-h-screen w-full flex flex-col justify-between items-center bg-white gap-10"
      id="why"
    >
      <Wave1 />
      <div className="flex flex-col justify-center items-center gap-20 pb-16">
        <h1 className="text-raspberry text-4xl font-bold border-b-4 border-gray px-4 py-2">
          Why CariPasien?
        </h1>
        <div className="flex justify-around gap-40 h-40">
          <WhyCard
            title="Terpercaya"
            detail="Dipercaya berbagai kalangan"
            icon={<TbShieldCheck strokeWidth={1} />}
          />
          <WhyCard
            title="Tersertifikasi"
            detail="Dokter gigi resmi tersertifikasi"
            icon={<TbCertificate strokeWidth={1} />}
          />
          <WhyCard
            title="Terakreditasi"
            detail="Diakui oleh pemerintah"
            icon={<TbLicense strokeWidth={1} />}
          />
          <WhyCard
            title="Terbaik"
            detail="Treatment dengan kualitas terbaik"
            icon={<TbMedal strokeWidth={1} />}
          />
        </div>
      </div>
      <Wave2 />
    </section>
  )
}
