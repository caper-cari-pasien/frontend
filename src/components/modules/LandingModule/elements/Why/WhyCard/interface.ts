import { ReactElement } from 'react'

export interface WhyCardProps {
  title?: string
  icon?: ReactElement
  detail?: string
}
