import { useElementOnScreen } from '@hooks'
import { cloneElement, useRef, useState } from 'react'
import { WhyCardProps } from './interface'

const WhyCard: React.FC<WhyCardProps> = ({ title, icon, detail }) => {
  const [hovered, setHovered] = useState(false)
  const ref = useRef<HTMLDivElement>(null)
  const isOnScreen = useElementOnScreen({ ref })

  const copiedIcon = icon
    ? cloneElement(icon, {
        className: `transition-all ${hovered ? 'w-20 h-20' : 'w-32 h-32'}`,
      })
    : undefined

  return (
    <div
      className={`flex flex-col items-center w-40 ${
        isOnScreen ? 'animate-fade-in-bottom' : 'invisible'
      }`}
      onMouseEnter={() => setHovered(true)}
      onMouseLeave={() => setHovered(false)}
      ref={ref}
    >
      {copiedIcon ? (
        copiedIcon
      ) : (
        <div className="rounded-full bg-[#EEEEEE] w-32 h-32" />
      )}
      <span
        className={`text-raspberry ${
          hovered ? 'py-0 text-md' : 'py-2 text-lg'
        }`}
      >
        {title}
      </span>
      <div className={`${hovered ? '' : 'hidden'}`}>
        <p
          className={`text-center text-lg transition-all ${
            hovered ? 'animate-why-in' : 'animate-why-out'
          }`}
        >
          {detail}
        </p>
      </div>
    </div>
  )
}

export default WhyCard
