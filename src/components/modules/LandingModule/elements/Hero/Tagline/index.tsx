import { useElementOnScreen } from '@hooks'
import { useRef } from 'react'

const Tagline: React.FC<React.ComponentPropsWithoutRef<'span'>> = ({
  children,
  className = '',
}) => {
  const ref = useRef<HTMLSpanElement>(null)
  const isOnScreen = useElementOnScreen({ ref })

  return (
    <span
      ref={ref}
      className={`${
        isOnScreen ? 'animate-slide-in-right' : 'invisible'
      } ${className}`}
    >
      {children}
    </span>
  )
}

export default Tagline
