import { Button } from '@elements'
import Image from 'next/image'
import Tagline from './Tagline'
import { useAuthContext } from '@contexts'
import Link from 'next/link'

export const Hero = () => {
  const { user } = useAuthContext()

  return (
    <section
      className="h-screen w-full pt-12 flex flex-col justify-center items-center bg-carribean"
      id="hero"
    >
      <div className="flex px-20 justify-center">
        <div className="flex flex-1 justify-center">
          <Image
            src="/images/landingpagephoto.png"
            alt=""
            width={600}
            height={600}
            priority={true}
          />
        </div>
        <div className="flex flex-1 flex-col justify-center items-center text-white gap-12">
          <div className="flex flex-col pl-20 gap-4">
            <div className="flex flex-col text-[64px] font-montserrat leading-tight">
              <Tagline>WE TAKE</Tagline>
              <Tagline className="animation-duration-[0.6s]">
                <span className="font-[800]">CARE</span> OF
              </Tagline>
              <Tagline className="animation-duration-[0.7s]">
                YOUR <span className="font-[800]">SMILE</span>
              </Tagline>
            </div>

            <div className="w-full border border-b-4 border-white"></div>

            <p className="text-lg">
              <span className="font-bold">CariPasien</span> merupakan aplikasi
              yang bertujuan untuk menjadi jembatan antara calon dokter yang
              membutuhkan pasien dan masyarakat yang membutuhkan treatment gigi.
            </p>
          </div>
          <Link
            className="w-full flex justify-center"
            href={
              user?.role === 'DOCTOR' ? '/treatment/open' : '/treatment/pesan'
            }
          >
            <Button className="bg-raspberry w-2/4 font-semibold">
              {user?.role === 'DOCTOR' ? 'Buka Treatment' : 'Book Treatment'}
            </Button>
          </Link>
        </div>
      </div>
    </section>
  )
}
