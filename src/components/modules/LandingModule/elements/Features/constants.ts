export const FEATURE = [
  {
    title: 'Daftar Treatment',
    description:
      'Lihat daftar treatment yang disediakan oleh dokter gigi yang bekerja sama dengan kami.',
    align: 'left',
    imageSrc: '/images/bukalowongan.png',
    buttonTitle: {
      DOCTOR: 'Lihat Treatment',
      PATIENT: 'Lihat Treatment',
    },
    buttonHref: {
      DOCTOR: '/treatment',
      PATIENT: '/treatment',
    },
  },
  {
    title: 'Ajukan Treatment',
    description:
      'Ajukan pembukaan treatment kepada para dokter gigi. Anda juga dapat menentukan lokasi dan waktu treatment dilakukan.',
    align: 'right',
    imageSrc: '/images/butuhtreatment.png',
    buttonTitle: {
      DOCTOR: 'Lihat Ajuan Treatment',
      PATIENT: 'Ajukan Treatment',
    },
    buttonHref: {
      DOCTOR: '/treatment/pesan',
      PATIENT: '/treatment/request',
    },
  },
  {
    title: 'Verifikasi Treatment',
    description:
      'Verifikasi dokter gigi yang akan merawat Anda agar proses dan hasil treatment sesuai ekspektasi Anda.',
    align: 'left',
    imageSrc: '/images/verifikasitreatment.png',
  },
  {
    title: 'Catatan Medis',
    description:
      'Catatan medis dapat membantu Anda memperoleh informasi lengkap mengenai riwayat treatment Anda.',
    align: 'right',
    imageSrc: '/images/catatanmedis.png',
  },
]
