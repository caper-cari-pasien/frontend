import { Button } from '@elements'
import { useElementOnScreen } from '@hooks'
import Link from 'next/link'
import { useRef } from 'react'
import { FeatureProps } from './interface'
import { HiOutlineChevronRight } from 'react-icons/hi'

const Feature: React.FC<FeatureProps> = ({
  title,
  description,
  align = 'left',
  image,
  buttonTitle,
  buttonHref,
}) => {
  const ref = useRef<HTMLDivElement>(null)
  const isOnScreen = useElementOnScreen({ ref, rootMargin: '100px 0px 50px' })

  return (
    <section
      className={`flex justify-center items-center gap-[6rem] h-screen text-white px-40 ${
        align == 'right' ? 'flex-row-reverse' : ''
      }`}
    >
      <div
        className={`flex flex-col flex-1 gap-4 ${
          isOnScreen
            ? align == 'left'
              ? 'animate-fade-in-left'
              : 'animate-fade-in-right'
            : 'invisible'
        }`}
      >
        <span className="font-bold text-5xl">{title}</span>
        <p className="w-[24rem] text-lg">{description}</p>
        {buttonTitle && buttonHref && (
          <Link href={buttonHref} className="w-fit">
            <Button rightIcon={<HiOutlineChevronRight />} className="px-8">
              {buttonTitle}
            </Button>
          </Link>
        )}
      </div>
      <div
        ref={ref}
        className={`flex-1 w-[40rem] h-[32rem] rounded-lg ${
          isOnScreen
            ? align == 'left'
              ? 'animate-fade-in-right'
              : 'animate-fade-in-left'
            : 'invisible'
        }`}
      >
        {image}
      </div>
    </section>
  )
}

export default Feature
