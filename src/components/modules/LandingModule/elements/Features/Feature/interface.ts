import { ReactNode } from 'react'

export interface FeatureProps {
  title: string
  description: string
  image?: ReactNode
  align?: 'left' | 'right'
  buttonTitle?: string
  buttonHref?: string
}
