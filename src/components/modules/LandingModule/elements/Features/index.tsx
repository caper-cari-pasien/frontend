import { useAuthContext } from '@contexts'
import Feature from './Feature'
import { FEATURE } from './constants'
import Image from 'next/image'

const Features = () => {
  const { user } = useAuthContext()

  return (
    <section className="bg-carribean flex flex-col items-center">
      <h1 className="text-white text-4xl font-bold border-b-4 border-raspberry px-4 py-2">
        Yang kami tawarkan
      </h1>
      {FEATURE.map(
        (
          { title, description, align, imageSrc, buttonTitle, buttonHref },
          idx
        ) => {
          return (
            <Feature
              key={idx}
              title={title}
              description={description}
              align={align as 'left' | 'right'}
              image={<Image src={imageSrc ?? ''} alt="" fill />}
              buttonTitle={buttonTitle && user ? buttonTitle[user.role!] : ''}
              buttonHref={buttonHref && user ? buttonHref[user.role!] : '/'}
            />
          )
        }
      )}
    </section>
  )
}

export { Features }
