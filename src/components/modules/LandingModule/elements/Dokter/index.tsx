import { useElementOnScreen } from '@hooks'
import Image from 'next/image'
import Link from 'next/link'
import { useRef } from 'react'

const Dokter = () => {
  const ref = useRef<HTMLImageElement>(null)
  const isOnScreen = useElementOnScreen({ ref })

  return (
    <section className="flex flex-col justify-center items-center h-screen bg-carribean gap-12">
      <Image
        ref={ref}
        src="/images/landingpagedokter.png"
        alt=""
        width={600}
        height={600}
        className={`${isOnScreen ? 'animate-fade-in-bottom' : 'invisible'}`}
      />
      <Link href="/dokter">
        <span className="text-white text-4xl font-bold border-b-2 border-white">
          Lihat Semua Dokter Kami
        </span>
      </Link>
    </section>
  )
}

export { Dokter }
