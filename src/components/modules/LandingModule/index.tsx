import { Features, Hero, Why } from './elements'
import { Dokter } from './elements/Dokter'

export const LandingModule = () => {
  return (
    <>
      <Hero />
      <Why />
      <Features />
      <Dokter />
    </>
  )
}
