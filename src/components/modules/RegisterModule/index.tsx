import { Button, InputText } from '@elements'
import Link from 'next/link'
import { SubmitHandler, useForm } from 'react-hook-form'
import { RegisterFormData } from './interface'
import { useAuthContext } from '@contexts'
import { useState } from 'react'
import { toast } from 'react-hot-toast'
import { useRouter } from 'next/router'

export const RegisterModule = () => {
  const { register, handleSubmit } = useForm<RegisterFormData>()
  const { httpReq } = useAuthContext()
  const [isRegistering, setIsRegistering] = useState(false)
  const router = useRouter()

  const handleRegister: SubmitHandler<RegisterFormData> = async (data) => {
    setIsRegistering(true)
    try {
      await httpReq({
        method: 'post',
        url: '/api/auth/register',
        body: data,
      })

      toast.success('Berhasil mendaftarkan, silakan Login')
      router.push('/login')
    } catch (e: any) {
      toast.error('Gagal mendaftarkan, cek lagi data diri Anda')
      console.log(e)
    }
    setIsRegistering(false)
  }

  return (
    <div className="h-screen bg-carribean py-20 px-40 flex place-content-center">
      <div className="flex flex-col justify-center">
        <form
          className="w-full bg-white rounded-3xl items-center py-16 px-36"
          onSubmit={handleSubmit(handleRegister)}
        >
          <p className="text-3xl font-semibold text-raspberry flex place-content-center pb-6">
            Register
          </p>
          <div className="flex flex-col md:flex-row gap-2 md:gap-20 pt-2 place-content-center">
            <InputText label="Nama" {...register('nama', { required: true })} />
            <InputText
              label="Username"
              {...register('username', { required: true })}
            />
          </div>
          <div className="flex flex-col md:flex-row gap-2 md:gap-20 pt-2 place-content-center">
            <InputText
              label="Password"
              type="password"
              {...register('password', { required: true })}
            />
            {/* <InputText label="Role" {...register('role', { required: true })} /> */}
            <div className="w-full flex flex-col">
              <span className="text-raspberry">Role</span>
              <select
                className="rounded-md border py-[0.1em] px-[0.5em]"
                {...register('role', { required: true })}
              >
                <option value="PATIENT">Pasien</option>
                <option value="DOCTOR">Dokter</option>
              </select>
            </div>
          </div>
          <div className="flex flex-col md:flex-row gap-2 md:gap-20 pt-2 place-content-center">
            <InputText
              label="Umur"
              type="number"
              {...register('umur', { required: true })}
            />
            <InputText
              label="Domisili"
              {...register('domisili', { required: true })}
            />
          </div>
          <div className="flex place-content-center pt-10">
            <Button
              className="bg-reaspberry text-white h-10 w-80"
              disabled={isRegistering}
            >
              Register
            </Button>
          </div>
          <div className="flex place-content-center pt-2">
            <Link href={'/login'}>
              <Button className="bg-sea-green text-white h-10 w-80">
                Already have an account? Login
              </Button>
            </Link>
          </div>
        </form>
      </div>
    </div>
  )
}
