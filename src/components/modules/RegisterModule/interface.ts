export interface RegisterFormData {
  nama: string
  password: string
  umur: number
  username: string
  role: 'DOCTOR' | 'PATIENT'
  domisili: string
}
