import { Button, InputText } from '@elements'
import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import Image from 'next/image'
import { DetailRow } from '../ProfileModule/elements'
import { useAuthContext } from '@contexts'
import { RequestTreatmentFormData } from './interface'
import { SubmitHandler, useForm } from 'react-hook-form'
import { toast } from 'react-hot-toast'

export const RequestTreatmentModule: React.FC = () => {
  const { register, handleSubmit } = useForm<RequestTreatmentFormData>()
  const { httpReq, user } = useAuthContext()
  const [currentUser, setCurrentUser] = useState(user)
  const router = useRouter()

  const handleRequestTreatment: SubmitHandler<
    RequestTreatmentFormData
  > = async (data) => {
    if (!user) return

    const [hour, minute] = data.treatmentTime.split(':')
    const date = new Date()
    date.setHours(parseInt(hour))
    date.setMinutes(parseInt(minute))
    date.setSeconds(0)

    data.treatmentTime = date.toISOString()
    data.patientUsername = user.username

    try {
      data['patientUsername'] = user.username

      await httpReq({
        method: 'post',
        url: '/api/treatment/pesan',
        body: data,
      })

      toast.success('Berhasil me-request treatment baru')
      router.push('/treatment/pesan')
    } catch (e: any) {
      toast.error('Gagal me-request treatment baru, cek lagi isi datanya')
      console.log(e)
    }
  }

  const fetchProfile = async () => {
    try {
      if (user) {
        const res = await httpReq({
          method: 'get',
          url: `/api/profile/pasien/${user.username}`,
        })
        console.log(res)
        setCurrentUser({ ...res.user })
      }
    } catch (e: any) {
      console.log(e)
    }
  }

  useEffect(() => {
    user && fetchProfile()
  }, [user])

  return (
    <>
      <div className="bg-carribean pt-32 py-12">
        <div className="flex items-start">
          <div className="sm flex flex-col w-2/5 items-center px-16 text-white">
            <Image
              src="/images/usericon.png"
              alt="user"
              width={150}
              height={150}
            />

            {/* Account detail */}

            <section className="flex flex-col items-center w-full gap-2 px-6">
              <h1 className="font-bold text-2xl my-4">{user?.nama}</h1>
              <div className="flex flex-col gap-4 w-full">
                <DetailRow
                  title="Username"
                  content={currentUser?.nama}
                  canEdited={false}
                />
                <DetailRow title="Domisili" content={currentUser?.domisili} />
                <DetailRow title="Umur" content={currentUser?.umur} />
              </div>
            </section>

            <div className="w-full border-b-2 border-white my-6"></div>

            {/* Foto gigi */}
            <section className="flex flex-col items-center w-full gap-2">
              <h1 className="font-bold text-2xl mb-2">Foto Gigi</h1>
              <div className="bg-[#EFEFEF] w-80 h-44 rounded-lg"></div>
            </section>
          </div>

          {/* Request Treatment */}
          <form
            className="flex flex-col w-3/5 bg-white rounded-lg grow py-12 px-12 mr-12 gap-3"
            onSubmit={handleSubmit(handleRequestTreatment)}
          >
            <div className="text-3xl flex justify-center mb-4">
              Ajukan Treatment
            </div>

            {/* <div className="text-lg">Treatment</div>
            <input
              type="text"
              className="w-full rounded-[7px] border border-black px-3 py-2.5"
              placeholder="Isi Treatment..."
            /> */}
            <InputText
              className="w-full rounded-[7px] px-3 py-2.5"
              label="Judul Treatment"
              type="text"
              {...register('title', { required: true })}
            />

            {/* <div className="text-lg mt-4">Keterangan Kondisi Pasien</div>
            <textarea
              className="min-h-[400px] w-full rounded-[7px] border border-black px-3 py-2.5"
              placeholder="Isi Keterangan Kondisi Pasien..."
            ></textarea> */}
            <InputText
              className="w-full rounded-[7px] px-3 py-2.5"
              label="Keterangan Kondisi Pasien"
              type="text"
              {...register('description', { required: true })}
            />

            {/* <div className="text-lg mt-4">Lokasi</div>
            <input
              type="text"
              className="w-full rounded-[7px] border border-black px-3 py-2.5"
              placeholder="Isi Lokasi..."
            /> */}
            <InputText
              className="w-full rounded-[7px] px-3 py-2.5"
              label="Lokasi"
              type="text"
              {...register('location', { required: true })}
            />

            {/* <div className="text-lg mt-4">Jam Treatment</div>
            <input
              type="text"
              className="w-full rounded-[7px] border border-black px-3 py-2.5"
              placeholder="Isi Jam Treatment..."
            /> */}
            <label>
              <span className="text-raspberry">Waktu Treatment</span>
              <input
                type="time"
                className="w-full rounded-[7px] px-3 py-2.5 border"
                {...register('treatmentTime', { required: true })}
              />
            </label>

            <Button className="bg-sea-green text-white shadow-lg w-full h-[50px] mt-4">
              Ajukan Treatment
            </Button>
          </form>
        </div>
      </div>
    </>
  )
}
