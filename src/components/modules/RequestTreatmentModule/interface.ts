export interface RequestTreatmentFormData {
  title: string
  location: string
  treatmentTime: string
  description: string
  patientUsername: string
}
