import React, { createContext, useContext, useEffect, useState } from 'react'
import {
  AuthContextProps,
  AuthProviderProps,
  HttpReqProps,
  User,
} from './interface'
import axios from 'axios'
import { parseJwt } from '@utils'
import { useRouter } from 'next/router'
import { validateJwt } from 'src/components/utils/validateJwt'
import { toast } from 'react-hot-toast'

const AuthContext = createContext<AuthContextProps | undefined>(undefined)

export const useAuthContext = () => {
  const context = useContext(AuthContext)

  if (context == undefined) {
    throw new Error('useAuthContext must be used within an AuthProvider')
  }

  return context
}

export const AuthProvider: React.FC<AuthProviderProps> = ({ children }) => {
  const [user, setUser] = useState<User | undefined>()
  const router = useRouter()

  const httpReq = async ({
    method,
    url,
    body,
    headers: additionalHeaders,
  }: HttpReqProps) => {
    const headers = {
      Authorization: '',
      ...additionalHeaders,
    }

    const token = localStorage.getItem('token')
    if (token !== null) {
      headers.Authorization = 'Bearer ' + token
    }

    const { data } = await axios({
      method,
      url: url,
      headers,
      data: method !== 'get' ? body : undefined,
      withCredentials: true,
    })

    return data
  }

  const contextValue = {
    user,
    setUser,
    httpReq,
  }

  const unprotected = ['/', '/login', '/register', '/dokter']
  const doctorOnly = ['/treatment/open']
  const patientOnly = ['/treatment/request']

  useEffect(() => {
    const valid = validateJwt()
    if (!valid && !unprotected.includes(router.asPath)) {
      if (user) {
        toast.error('Sesi Anda telah habis. Silakan Login kembali')
      } else {
        toast.error('Silakan Login terlebih dahulu')
      }
      setUser(undefined)
      localStorage.removeItem('token')
      router.replace('/login')
    } else if (valid) {
      const token = localStorage.getItem('token')
      const parsed = parseJwt(token!)
      setUser({ ...parsed } as User)
    }
  }, [router.asPath])

  useEffect(() => {
    if (user && router.asPath) {
      if (user.role === 'DOCTOR' && patientOnly.includes(router.asPath)) {
        toast.error('Halaman ini tidak tersedia untuk Anda')
        router.back()
      }
      if (user.role === 'PATIENT' && doctorOnly.includes(router.asPath)) {
        toast.error('Halaman ini tidak tersedia untuk Anda')
        router.back()
      }
    }
  }, [user])

  return (
    <AuthContext.Provider value={contextValue}>{children}</AuthContext.Provider>
  )
}
