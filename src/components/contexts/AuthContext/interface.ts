import { ReactNode } from 'react'

export interface AuthProviderProps {
  children: ReactNode
}

export interface AuthContextProps {
  user: User | undefined
  setUser: (user: User | undefined) => void
  httpReq: ({ method, url, body }: HttpReqProps) => Promise<any>
}

export interface HttpReqProps {
  method: 'get' | 'post' | 'put' | 'delete' | 'patch'
  url: string
  body?: any
  headers?: any
}

export type User = {
  id: string
  nama: string
  username: string
  role?: RoleEnum
  umur?: number
  domisili?: string
  certificateUrl?: string
}

type RoleEnum = 'DOCTOR' | 'PATIENT'
