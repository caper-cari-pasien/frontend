import React, { createContext, useContext, useState } from 'react'
import {
  DokterInfo,
  DokterListContextProps,
  DokterListProviderProps,
} from './interface'

const DokterListContext = createContext<DokterListContextProps | undefined>(
  undefined
)

export const useDokterListContext = () => {
  const context = useContext(DokterListContext)

  if (context == undefined) {
    throw new Error(
      'useDokterListContext must be used within an DokterListProvider'
    )
  }

  return context
}

export const DokterListContextProvider: React.FC<DokterListProviderProps> = ({
  children,
}) => {
  const [dokterList, setDokterList] = useState<DokterInfo[]>([])
  const [domisiliList, setDomisiliList] = useState<string[]>([])
  const [selectedDomisiliFilter, setSelectedDomisiliFilter] = useState<string>()

  const updateDokterList = (newList: DokterInfo[]) => {
    setDokterList([...newList])
    const domisiliSet = new Set(newList.map((e) => e.domisili))
    setDomisiliList(Array.from(domisiliSet))
  }

  const contextValue = {
    dokterList,
    setDokterList: updateDokterList,
    domisiliList,
    selectedDomisiliFilter,
    setSelectedDomisiliFilter,
  }

  return (
    <DokterListContext.Provider value={contextValue}>
      {children}
    </DokterListContext.Provider>
  )
}
