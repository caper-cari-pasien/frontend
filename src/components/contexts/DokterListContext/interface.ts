import { ReactNode } from 'react'

export interface props {}

export interface DokterListProviderProps {
  children: ReactNode
}

export interface DokterListContextProps {
  dokterList: DokterInfo[]
  setDokterList: (newList: DokterInfo[]) => void
  domisiliList: string[]
  selectedDomisiliFilter?: string
  setSelectedDomisiliFilter: (selected?: string) => void
}

export type DokterInfo = {
  nama: string
  domisili: string
  umur: number
}
