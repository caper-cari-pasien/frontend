import { RefObject } from 'react'

export interface ElementOnScreenProps {
  ref: RefObject<HTMLElement>
  rootMargin?: string
}
