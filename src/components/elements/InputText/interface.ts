export interface StyledInputTextProps {
  isError?: boolean
}

export interface InputTextProps extends StyledInputTextProps {
  label?: string
  labelClassName?: string
  type?: 'text' | 'password' | 'email' | 'number'
}
