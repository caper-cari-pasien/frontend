import { ComponentPropsWithoutRef } from 'react'
import styled from 'styled-components'
import { StyledInputTextProps } from './interface'

export const StyledInputText = styled.input<
  StyledInputTextProps & ComponentPropsWithoutRef<'input'>
>`
  padding: 0.1em 0.5em 0.1em 0.5em;
  border: 0.05em solid;
  border-color: ${({ isError }) => (isError ? '#D81159' : 'black')};
  border-radius: 0.3em;
  color: black;

  :focus {
    border-color: #0ca4a5;
    outline: 1px solid #0ca4a5;
  }

  :disabled {
    background-color: #efefef;
    color: #6d6d6d;
  }

  ::-webkit-inner-spin-button,
  ::-webkit-outer-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
`

export const StyledInputLabel = styled.label`
  display: flex;
  flex-direction: column;
  color: #d81159;
  width: full;
  flex-grow: 1;
`
