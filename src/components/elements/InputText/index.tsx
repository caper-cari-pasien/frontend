import { ComponentPropsWithoutRef, forwardRef } from 'react'
import { InputTextProps } from './interface'
import { StyledInputLabel, StyledInputText } from './InputText.style'

export const InputText = forwardRef<
  HTMLInputElement,
  InputTextProps & ComponentPropsWithoutRef<'input'>
>(({ label, labelClassName, isError, type = 'text', ...props }, ref) => {
  return (
    <StyledInputLabel>
      <span className={labelClassName}>{label}</span>
      <StyledInputText ref={ref} type={type} isError={isError} {...props} />
    </StyledInputLabel>
  )
})

InputText.displayName = 'InputText'
