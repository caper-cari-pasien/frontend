import React from 'react'

export const Footer: React.FC = () => {
  return (
    <footer className="w-full font-roboto bg-sea-green flex justify-center py-6 text-white">
      Copyright © 2023 CariPasien
    </footer>
  )
}
