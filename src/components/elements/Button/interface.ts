import { ReactNode } from 'react'

export interface StyledContainerProps {
  disabled?: boolean
}

export interface ButtonProps {
  leftIcon?: ReactNode
  rightIcon?: ReactNode
  disabled?: boolean
}
