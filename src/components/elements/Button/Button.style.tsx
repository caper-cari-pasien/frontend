import styled from 'styled-components'
import { StyledContainerProps } from './interface'

export const StyledButton = styled.button`
  text-align: center;
  flex-grow: 1;
`

export const Container = styled.div<
  StyledContainerProps & React.ComponentPropsWithoutRef<'div'>
>`
  padding: 0.5em 1em 0.5em 1em;
  border-radius: 0.5em;
  background-color: #d81159;
  display: flex;
  gap: 0.5rem;
  width: fit-content;
  align-items: center;
  cursor: ${(props) => (props.disabled ? 'default' : 'pointer')};

  transition: filter 1000ms cubic-bezier(0.075, 0.82, 0.165, 1) 0ms;
  -webkit-transition: -webkit-filter 1000ms cubic-bezier(0.075, 0.82, 0.165, 1)
    0ms;

  filter: ${(props) =>
    props.disabled ? 'brightness(70%)' : 'brightness(100%)'};

  :hover {
    filter: ${(props) =>
      props.disabled ? 'brightness(70%)' : 'brightness(115%)'};
  }

  :active {
    filter: ${(props) =>
      props.disabled ? 'brightness(70%)' : 'brightness(90%)'};
  }
`
