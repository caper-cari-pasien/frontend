import React, { ComponentPropsWithoutRef } from 'react'
import { Container, StyledButton } from './Button.style'
import { ButtonProps } from './interface'

export const Button: React.FC<
  ButtonProps & ComponentPropsWithoutRef<'button'>
> = ({
  leftIcon,
  rightIcon,
  disabled,
  className,
  children,
  onClick,
  ...props
}) => {
  return (
    <Container className={`${className}`} disabled={disabled}>
      {leftIcon}
      <StyledButton onClick={onClick} disabled={disabled} {...props}>
        {children}
      </StyledButton>
      {rightIcon}
    </Container>
  )
}
