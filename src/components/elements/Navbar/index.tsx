import { useAuthContext } from '@contexts'
import { Button } from '@elements'
import { useScrollPosition } from '@hooks'
import Link from 'next/link'
import React from 'react'
import { ProfileDropdown } from './ProfileDropdown'

export const Navbar: React.FC = () => {
  const scrollPosition = useScrollPosition()
  const { user } = useAuthContext()

  return (
    <header
      className={`top-0 left-0 z-10 w-full fixed bg-carribean text-white flex justify-between items-center py-6 px-20 font-inter
    ${scrollPosition > 0 ? 'shadow-md' : ''}`}
    >
      <div className="flex gap-8">
        <h1 className="text-4xl tracking-[0.1em] font-inconsolata">
          <span className="font-semibold">CARI</span>
          <span className="font-light">PASIEN</span>
        </h1>

        <nav className="flex gap-1">
          <li className="flex justify-center align-center px-4 py-2 rounded-lg">
            <Link href="/">
              <span className="font-semibold text-lg text-center">Home</span>
            </Link>
          </li>
          <li className="flex justify-center align-center px-4 py-2 rounded-lg">
            <Link href="/dokter">
              <span className="font-semibold text-lg text-center">
                List Dokter
              </span>
            </Link>
          </li>
          {user && (
            <>
              <li className="flex justify-center align-center px-4 py-2 rounded-lg">
                <Link href="/treatment/ongoing">
                  <span className="font-semibold text-lg text-center">
                    Treatment Anda
                  </span>
                </Link>
              </li>
              <li className="flex justify-center align-center px-4 py-2 rounded-lg">
                <Link href="/treatment">
                  <span className="font-semibold text-lg text-center">
                    List Treatment
                  </span>
                </Link>
              </li>
            </>
          )}
          {user?.role === 'DOCTOR' && (
            <>
              <li className="flex justify-center align-center px-4 py-2 rounded-lg">
                <Link href="/treatment/open">
                  <span className="font-semibold text-lg text-center">
                    Buka Treatment
                  </span>
                </Link>
              </li>
              <li className="flex justify-center align-center px-4 py-2 rounded-lg">
                <Link href="/treatment/pesan">
                  <span className="font-semibold text-lg text-center">
                    Ajuan Treatment
                  </span>
                </Link>
              </li>
            </>
          )}
          {user?.role === 'PATIENT' && (
            <li className="flex justify-center align-center px-4 py-2 rounded-lg">
              <Link href="/treatment/request">
                <span className="font-semibold text-lg text-center">
                  Ajukan Treatment
                </span>
              </Link>
            </li>
          )}
        </nav>
      </div>
      <div className="flex relative gap-4">
        {user === undefined ? (
          <>
            <Link href="/login">
              <Button className="bg-raspberry w-24">Login</Button>
            </Link>
            <Link href="/register">
              <Button className="bg-raspberry w-24">Register</Button>
            </Link>
          </>
        ) : (
          <ProfileDropdown name={user.nama} username={user.username} />
        )}
      </div>
    </header>
  )
}
