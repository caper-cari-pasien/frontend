import { FaUserCircle } from 'react-icons/fa'
import { ProfileDropdownProps } from './interface'
import { FiChevronDown, FiLogOut } from 'react-icons/fi'
import { useState } from 'react'
import { Button } from '@elements'
import Link from 'next/link'
import { useAuthContext } from '@contexts'
import { toast } from 'react-hot-toast'
import { useRouter } from 'next/router'

export const ProfileDropdown: React.FC<ProfileDropdownProps> = ({
  name,
  username,
}) => {
  const [isOpen, setIsOpen] = useState(false)
  const { httpReq, setUser } = useAuthContext()
  const router = useRouter()

  const logout = async () => {
    await httpReq({
      method: 'post',
      url: '/api/auth/logout',
    })

    setUser(undefined)
    localStorage.removeItem('token')

    toast.success('Berhasil logout')

    router.push('/')
  }

  return (
    <>
      <div
        className="flex gap-2 items-center cursor-pointer"
        onClick={() => setIsOpen(!isOpen)}
      >
        <FaUserCircle className="w-10 h-10" />
        <FiChevronDown className={`w-6 h-6 ${isOpen && 'rotate-180'}`} />
      </div>
      {isOpen && (
        <div
          className="flex flex-col items-start absolute rounded-lg border border-black bg-white text-black
          px-4 py-2 top-14 -left-10 w-40"
        >
          <span className="font-bold">{name}</span>
          <span>@{username}</span>
          <div className="border-t-2 border-black my-2 w-full" />
          <Link
            href={`/profile/${username}`}
            className="w-full rounded-lg px-1 hover:bg-black/10 active:bg-black/20"
          >
            <span>Your Profile</span>
          </Link>
          <div className="border-t-2 border-black my-2 w-full" />
          <Button
            className="bg-transparent hover:bg-black/10 active:bg-black/20"
            leftIcon={<FiLogOut />}
            onClick={logout}
          >
            Sign Out
          </Button>
        </div>
      )}
    </>
  )
}
