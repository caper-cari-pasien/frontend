export interface ProfileDropdownProps {
  name: string
  username: string
}
