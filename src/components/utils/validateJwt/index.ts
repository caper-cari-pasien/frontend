import { parseJwt } from '../parseJwt'

export const validateJwt = () => {
  const token = localStorage.getItem('token')
  if (token !== null) {
    const parsed = parseJwt(token)
    return parsed.exp * 1000 > Date.now()
  }

  return false
}
