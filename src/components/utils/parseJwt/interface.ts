export interface ParseJwtInterface {
  exp: number
  iat: number
  sub: string
  username: string
  nama: string
  role: 'DOCTOR' | 'PATIENT'
}
