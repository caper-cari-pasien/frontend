const plugin = require('tailwindcss/plugin')

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        carribean: '#006d77',
        'sea-green': '#0CA4A5',
        tiffany: '#83C5BE',
        gray: '#7B7E7F',
        aquamarine: '#5DFDCB',
        nyanza: '#E1FFE8',
        raspberry: '#D81159',
        folly: '#F52F57',
      },
      fontFamily: {
        poppins: 'Poppins',
        inconsolata: 'Inconsolata',
        inter: 'Inter',
        roboto: 'Roboto',
        montserrat: 'Montserrat',
      },
      transitionProperty: {
        filter: 'filter, -webkit-filter',
      },
      keyframes: {
        'why-in': {
          '0%': {
            height: 0,
            opacity: 0,
          },
          '50%': {
            height: 'auto',
          },
          '100%': {
            height: 'auto',
            opacity: 1,
          },
        },
        'why-out': {
          '0%': {
            height: 'auto',
            opacity: 1,
          },
          '50%': {
            opacity: 0,
          },
          '100%': {
            height: 0,
            opacity: 0,
          },
        },
        'slide-in-right': {
          '0%': {
            opacity: 0,
            transform: 'translateX(50%)',
          },
          '100%': {
            opacity: 1,
            transform: 'translateX(0)',
          },
        },
        'fade-in-bottom': {
          '0%': {
            opacity: 0,
            transform: 'translateY(20%)',
          },
          '100%': {
            opacity: 1,
            transform: 'translateY(0)',
          },
        },
        'fade-in-left': {
          '0%': {
            opacity: 0,
            transform: 'translateX(-20%)',
          },
          '100%': {
            opacity: 1,
            transform: 'translateX(0)',
          },
        },
        'fade-in-right': {
          '0%': {
            opacity: 0,
            transform: 'translateX(20%)',
          },
          '100%': {
            opacity: 1,
            transform: 'translateX(0)',
          },
        },
      },
      animation: {
        'why-in': 'why-in 0.5s forwards',
        'why-out': 'why-out 0.5s forwards',
        'slide-in-right': 'slide-in-right 0.5s ease-out forwards',
        'fade-in-bottom': 'fade-in-bottom 0.8s ease-out forwards',
        'fade-in-left': 'fade-in-left 0.8s ease-out forwards',
        'fade-in-right': 'fade-in-right 0.8s ease-out forwards',
      },
    },
  },
  plugins: [
    plugin(({ matchUtilities, theme }) => {
      matchUtilities(
        {
          'animation-delay': (value) => {
            return {
              'animation-delay': value,
            }
          },
          'animation-duration': (value) => {
            return {
              'animation-duration': value,
            }
          },
        },
        {
          values: theme('transitionDelay'),
        }
      )
    }),
  ],
}
